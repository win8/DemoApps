

[ Windows Store App submission checklist ]
http://msdn.microsoft.com/en-us/library/windows/apps/hh694062.aspx


## Description 

A "symbol icon" demo. This app demonstrates the use of XAML <SymbolIcon/> control.

Note: This is an example app from a series of Windows 8/8.1 training apps intended for beginning developers. An improved version of this app is now available as a paid app with a non-expiring free trial. Search for "Icon Browser" on Windows and Windows Phone app store. Icon Browser provides more features than this demo app even in the trial mode.


## Release note

v1.2.0
(1) Updated the XAML code snippets. 
(2) Implemented Copy-to-Clipboard feature to copy XAML snippets to system clipboard (Windows only). On Windows Phone 8.1, the same action opens a "Share UI". 


## Screenshots

1366 x 768

SymbolIcon list.



## App Features



## Keywords

Symbol icons.
Windows universal app development
Windows Phone 8.1 development


## Copyright & Trademark

Copyright (c) 2015, Holo Software



## App website

http://www.windowsdemoapps.com/

## Support contact info

http://www.windowsdemoapps.com/support
holosoftware@outlook.com


## Privacy policy

http://www.windowsdemoapps.com/privacy


## Legal

http://www.windowsdemoapps.com/terms
http://www.windowsdemoapps.com/legal



