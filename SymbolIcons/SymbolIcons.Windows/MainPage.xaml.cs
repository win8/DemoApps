﻿using HoloBase.Toast;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SymbolIcons
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            UserControlSymbolIconGrid.TextClipAvailable += UserControlSymbolIconGrid_TextClipAvailable;
        }
        void UserControlSymbolIconGrid_TextClipAvailable(object sender, HoloBase.Controls.Core.TextClipEventArgs e)
        {
            var textClip = e.TextClip;
            System.Diagnostics.Debug.WriteLine("UserControlSymbolIconGrid_TextClipAvailable() textClip = {0}", textClip);

            if (!String.IsNullOrEmpty(textClip)) {
                DataPackage dataPackage = new DataPackage();
                dataPackage.SetText(textClip);
                Clipboard.SetContent(dataPackage);
                InstantToastHelper.Instance.ShowToast("XAML snippets copied to system clipboard.");
            }
        }


        private void ViewboxSourceCodeIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((App) App.Current).ShowSourceCodeInfoSettings();
        }
        private void AppBarButtonSourceCodeInfo_Click(object sender, RoutedEventArgs e)
        {
            ((App) App.Current).ShowSourceCodeInfoSettings();
        }
    }
}
