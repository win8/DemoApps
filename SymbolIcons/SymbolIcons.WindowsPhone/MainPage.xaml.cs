﻿using HoloBase.Share;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SymbolIcons
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            UserControlSymbolIconList.TextClipAvailable += UserControlSymbolIconList_TextClipAvailable;
        }
        void UserControlSymbolIconList_TextClipAvailable(object sender, HoloBase.Controls.Core.TextClipEventArgs e)
        {
            var textClip = e.TextClip;
            System.Diagnostics.Debug.WriteLine("UserControlSymbolIconList_TextClipAvailable() textClip = {0}", textClip);

            // InstantToastHelper.Instance.ShowToast("Windows Phone 8.1 does not allow system clipboard access. Text selected: " + textClip);
            ShareText(textClip, "XAML Snippets (Symbol Icons)");
        }
        // temporary
        private void ShareText(string text, string title = null)
        {
            if (String.IsNullOrEmpty(title)) {
                title = "Symbol Icons";
            }
            var textShareHandler = new TextShareHandler(title);
            textShareHandler.InitiateShare(text);
            DataTransferManager.ShowShareUI();
        }
 

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void ViewboxSourceCodeIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((App) App.Current).ShowSourceCodeInfoSettings();
        }
        private void AppBarButtonSourceCodeInfo_Click(object sender, RoutedEventArgs e)
        {
            ((App) App.Current).ShowSourceCodeInfoSettings();
        }
    }
}
