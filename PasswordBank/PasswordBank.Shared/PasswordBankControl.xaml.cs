﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PasswordBank
{
    public sealed partial class PasswordBankControl : UserControl
    {
        public PasswordBankControl()
        {
            this.InitializeComponent();

#if WINDOWS_PHONE_APP
            UserControlSimplePwordGenerator.IsPwordCopyButtonVisible = false;
#else
            UserControlSimplePwordGenerator.IsPwordCopyButtonVisible = true;
#endif

            UserControlSimplePwordGenerator.TextClipAvailable += UserControlSimplePwordGenerator_TextClipAvailable;
        }

        void UserControlSimplePwordGenerator_TextClipAvailable(object sender, HoloBase.Controls.Core.TextClipEventArgs e)
        {
#if WINDOWS_APP
            var pword = e.TextClip;
            System.Diagnostics.Debug.WriteLine("UserControlSimplePwordGenerator_TextClipAvailable(): pword = {0}", pword);
            if (!String.IsNullOrEmpty(pword)) {
                Windows.ApplicationModel.DataTransfer.DataPackage dataPackage = new Windows.ApplicationModel.DataTransfer.DataPackage();
                dataPackage.SetText(pword);
                Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(dataPackage);
            }
#endif
        }

    }
}
