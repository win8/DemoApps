SETLOCAL EnableDelayedExpansion
REM SET IMCONV="C:\Apps\ImageMagick-6.9.0-Q16\Convert"
SET IMCONV="C:\Programs\ImageMagick-6.9.0-Q16\Convert"

REM for %%f in (*.bmp) do (
REM     %IMCONV% "%%~nf.bmp" -resize 100x150 -quality 100 "%%~nf-100x150.scale-100.png"
REM     %IMCONV% "%%~nf.bmp" -resize 140x210 -quality 100 "%%~nf-100x150.scale-140.png"
REM     %IMCONV% "%%~nf.bmp" -resize 180x270 -quality 100 "%%~nf-100x150.scale-180.png"
REM REM    %IMCONV% "%%~nf.bmp" -resize 240x360 -quality 100 "%%~nf-100x150.scale-240.png"
REM )


%IMCONV% "logo1000.png" -resize 24x24 -quality 100 "smalllogo.scale-100.png"
%IMCONV% "logo1000.png" -resize 40x40 -quality 100 "storelogo.scale-100.png"
%IMCONV% "logo1000.png" -resize 70x70 -quality 100 "mainlogo-widelogo.scale-100.png"
%IMCONV% "logo1000.png" -resize 214x214 -quality 100 "splashscreen.scale-100.png"

%IMCONV% "logo1000.png" -resize 84x84 -quality 100 "smalllogo.scale-240.png"
%IMCONV% "logo1000.png" -resize 96x96 -quality 100 "storelogo.scale-240.png"
%IMCONV% "logo1000.png" -resize 168x168 -quality 100 "mainlogo-widelogo.scale-240.png"
%IMCONV% "logo1000.png" -resize 240x240 -quality 100 "splashscreen.scale-240.png"
%IMCONV% "logo1000.png" -resize 136x136 -quality 100 "apptitle-square71.scale-240.png"

%IMCONV% "logo1000-alpha-inverse.png" -resize 24x24 -quality 100 "BadgeLogo.scale-100.png"
%IMCONV% "logo1000-alpha-inverse.png" -resize 33x33 -quality 100 "BadgeLogo.scale-140.png"
%IMCONV% "logo1000-alpha-inverse.png" -resize 43x43 -quality 100 "BadgeLogo.scale-180.png"
%IMCONV% "logo1000-alpha-inverse.png" -resize 58x58 -quality 100 "BadgeLogo.scale-240.png"


