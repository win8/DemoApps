﻿using HoloBase.UI.Colors;
using HoloBase.UI.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ColorPicker
{
    public sealed partial class ColorDisplayControl : UserControl
    {
        // temporary
        private Size currentPageSize = Size.Empty;

        public ColorDisplayControl()
        {
            this.InitializeComponent();
            this.SizeChanged += ColorDisplayControl_SizeChanged;

            UserControlSimpleNamedColorPicker.ColorSelectionChanged += UserControlSimpleNamedColorPicker_ColorSelectionChanged;
        }


        // temporary
        public double PickerWidth
        {
            get
            {
                return UserControlSimpleNamedColorPicker.Width;
            }
            set
            {
                UserControlSimpleNamedColorPicker.Width = value;
            }
        }
        public double PickerHeight
        {
            get
            {
                return UserControlSimpleNamedColorPicker.Height;
            }
            set
            {
                UserControlSimpleNamedColorPicker.Height = value;
            }
        }
        // temporary

        
        
        void ColorDisplayControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentPageSize = e.NewSize;
        }

        void UserControlSimpleNamedColorPicker_ColorSelectionChanged(object sender, ColorSelectionChangedEventArgs e)
        {
            // var namedColor = UserControlSimpleNamedColorPicker.CurrentSelectedColor;
            // var colorStruct = new ColorStruct(namedColor);
            var colorStruct = e.SelectedColor;

            TextBoxColorName.Text = colorStruct.ColorName;
            TextBoxColorArgb.Text = colorStruct.ARGB;
            RectangleColorDisplay.Fill = new SolidColorBrush(colorStruct.Color);
        }

        private void ButtonPickColorName_Click(object sender, RoutedEventArgs e)
        {
            // tbd.
            if (!PopupSimpleNamedColorPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupSimpleNamedColorPicker.HorizontalOffset = (currentPageSize.Width - 240) / 2.0;
                    PopupSimpleNamedColorPicker.VerticalOffset = 30;
                }
                PopupSimpleNamedColorPicker.IsOpen = true;
            }
            // ..
        }
    }
}
