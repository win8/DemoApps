﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;
#if WINDOWS_APP
using HoloQR.Windows.Decode;
using HoloQR.Windows.Pickers;
#endif
#if WINDOWS_PHONE_APP
using HoloQR.WindowsPhone.Decode;
using HoloQR.WindowsPhone.Pickers;
#endif
using HoloBase.Toast;
using HoloCore.Core;
using HoloQR.Pickers;



namespace QrCode
{
    public sealed partial class QrReaderControl : UserControl, IRefreshableElement
    {
        // temporary
        private static readonly string inputImageFileOpenPickerSettingsIdentifier = "inputImageFileOpenPicker";

        private PickerLocationId defaultInputImageFileFolderId;

        // Keep the "state" here (primarily for WP81 file open picker process)
        private IStorageFile inputImageFile;
        // ...

        //// temporary
        //// Reference to the parent.
        //private FlipView parentFlipView = null;

        public QrReaderControl()
        {
            this.InitializeComponent();

            // temporary
            defaultInputImageFileFolderId = PickerLocationId.ComputerFolder;
            // defaultInputImageFileFolderId = PickerLocationId.Downloads;

            inputImageFile = null;
            ButtonReset.IsEnabled = false;
            ButtonScan.IsEnabled = false;
            TextBoxResult.Text = "";

#if WINDOWS_PHONE_APP
            // ButtonTakePicture.IsEnabled = true;
            ButtonTakePicture.IsEnabled = false;
#else
            ButtonTakePicture.IsEnabled = false;
#endif
        }

        //// TBD:
        //public void SetParentView(ItemsControl control)
        //{
        //    parentFlipView = (FlipView) control;
        //}

        public void RefreshDataAndUI()
        {
            // ...
        }

        private IStorageFile GetInputImageFile()
        {
            return inputImageFile;
        }
        private async Task SetInputImageFile(IStorageFile imageFile)
        {
            inputImageFile = imageFile;
            if (inputImageFile == null) {
                ButtonReset.IsEnabled = false;
                ButtonScan.IsEnabled = false;
                ImageQrCodeImage.Source = null;
            } else {
                // ???
                ButtonReset.IsEnabled = true;
                ButtonScan.IsEnabled = true;
                using (Windows.Storage.Streams.IRandomAccessStream fileStream =
                    await inputImageFile.OpenAsync(Windows.Storage.FileAccessMode.Read)) {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.SetSource(fileStream);
                    ImageQrCodeImage.Source = bitmapImage;
                }
            }
            TextBoxResult.Text = "";
        }


        private async void ButtonPickFile_Click(object sender, RoutedEventArgs e)
        {
            var qrCodeImageFile = await InputQrImagePickerHelper.Instance.ChooseQrImageInputFileAsync(inputImageFileOpenPickerSettingsIdentifier, defaultInputImageFileFolderId, FileOpenPickerContext.QrImageInputFile);
#if WINDOWS_PHONE_APP
#else
            DoSettingInputImageFile(qrCodeImageFile);
#endif
        }
        public async Task DoSettingInputImageFile(IStorageFile qrCodeImageFile)
        {
            await SetInputImageFile(qrCodeImageFile);
        }

        private void ButtonTakePicture_Click(object sender, RoutedEventArgs e)
        {

        }

        
        private async void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            await SetInputImageFile(null);
            TextBoxResult.Text = "";
            // etc...
        }

        private async void ButtonScan_Click(object sender, RoutedEventArgs e)
        {
            //// testing
            //System.Diagnostics.Debug.WriteLine("Package.Current.InstalledLocation = {0}", Package.Current.InstalledLocation.Path);
            //var bitmapFile = await Package.Current.InstalledLocation.GetFileAsync("test-qrcode.png");

            if (inputImageFile != null) {
                var qrCode = await QrDecodeHelper.Instance.ReadQrCodeImageAsync(inputImageFile);
                if (qrCode != null) {
                    System.Diagnostics.Debug.WriteLine("Successfully scanned QR Code image = {0}, qrCode = {1}", inputImageFile.Path, qrCode.Content);
                    TextBoxResult.Text = qrCode.Content;
                    InstantToastHelper.Instance.ShowToast("Successfully scanned QR Code image.");
                } else {
                    System.Diagnostics.Debug.WriteLine("Failed to read QR Code image: bitmapFile = {0}", inputImageFile.Path);
                    TextBoxResult.Text = "_failed_";
                    InstantToastHelper.Instance.ShowToast("Failed to read QR Code image.");
                }
            } 
            // camera ??

        }

     }
}
