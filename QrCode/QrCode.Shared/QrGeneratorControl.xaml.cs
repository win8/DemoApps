﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
#if WINDOWS_APP
using HoloQR.Windows.Encode;
using HoloQR.Windows.Pickers;
#endif
#if WINDOWS_PHONE_APP
using HoloQR.WindowsPhone.Encode;
using HoloQR.WindowsPhone.Pickers;
#endif
using HoloBase.Toast;
using Windows.Storage.Streams;
using Windows.Graphics.Imaging;
using System.Threading.Tasks;
using HoloQR.Common;
using HoloBase.UI.Colors;
using HoloQR.Data;
using HoloCore.Core;
using HoloQR.Pickers;



namespace QrCode
{
    public sealed partial class QrGeneratorControl : UserControl, IRefreshableElement
    {
        private static readonly string encodedImageFileFolderPickerSettingsIdentifier = "encodedImageFileFolderPicker";
        private static readonly string encodedImageFileSavePickerSettingsIdentifier = "encodedImageFileSavePicker";

        // private IStorageFolder defaultOutputQrCodeImageFileFolder;
        private IStorageFolder outputQrCodeImageFileFolder;

        // tmp
        private WriteableBitmap currentOutputImage;

        //// temporary
        //// Reference to the parent.
        //private FlipView parentFlipView = null;

        public QrGeneratorControl()
        {
            this.InitializeComponent();

            outputQrCodeImageFileFolder = ApplicationData.Current.TemporaryFolder;
            // TextBoxEncodedImageFileFolder.Text = outputQrCodeImageFileFolder.Name;

            currentOutputImage = null;

            TextBoxInputContent.Text = "";
            ButtonGenerate.IsEnabled = false;
            ButtonSaveImage.IsEnabled = false;
        }

        //// TBD:
        //public void SetParentView(ItemsControl control)
        //{
        //    parentFlipView = (FlipView) control;
        //}

        public void RefreshDataAndUI()
        {
            if (PopupQrCodeEncodeOptions != null && PopupQrCodeEncodeOptions.IsOpen) {
                PopupQrCodeEncodeOptions.IsOpen = false;
            }
        }

        private async void ButtonOutputFolder_Click(object sender, RoutedEventArgs e)
        {
            var folder = await QrImageOutputPickerHelper.Instance.ChooseFolderAsync(encodedImageFileFolderPickerSettingsIdentifier, FolderPickerContext.QrImageOutputFolder);
#if WINDOWS_PHONE_APP
#else
            DoChangeOutputImageFolder(folder);
#endif
        }
        public void DoChangeOutputImageFolder(IStorageFolder folder)
        {
            if (folder != null) {
                outputQrCodeImageFileFolder = folder;
                // TextBoxEncodedImageFileFolder.Text = outputQrCodeImageFileFolder.Name;
            } else {
                // ???
            }
        }

        private async void ButtonSaveImage_Click(object sender, RoutedEventArgs e)
        {
            if (currentOutputImage != null) {
                //if (outputQrCodeImageFileFolder != null) {
                //    var folderPath = outputQrCodeImageFileFolder.Path;
                //    var fileName = OutputFileNameUtil.GenerateSuggestedOutputFileName();
                //    var filePath = folderPath + @"\" + fileName;
                //    var outputFile = await StorageFile.GetFileFromPathAsync(filePath);
                //    if (outputFile != null) {

                //    }
                //}
                var fileName = OutputFileNameUtil.GenerateSuggestedOutputFileName();
                var file = await QrImageOutputPickerHelper.Instance.ChooseOutputFileAsync(encodedImageFileSavePickerSettingsIdentifier, fileName, FileSavePickerContext.QrImageOutputFile);
#if WINDOWS_PHONE_APP
#else
                await DoSaveImageUponFilePick(file);
#endif
            }
        }
        public async Task DoSaveImageUponFilePick(IStorageFile outputFile)
        {
            if (outputFile != null) {
                if (currentOutputImage != null) {

                    var fileType = outputFile.FileType;
                    System.Diagnostics.Debug.WriteLine("fileType = {0}", fileType);

                    // var bitmapEncoderGuid = BitmapEncoder.PngEncoderId;
                    var bitmapEncoderGuid = BitmapFileTypes.GetBitmapEncoderGuid(fileType);
                    using (IRandomAccessStream stream = await outputFile.OpenAsync(FileAccessMode.ReadWrite)) {
                        BitmapEncoder encoder = await BitmapEncoder.CreateAsync(bitmapEncoderGuid, stream);
                        // ???
                        //encoder.IsThumbnailGenerated = true;
                        //encoder.GeneratedThumbnailWidth = 60;
                        //encoder.GeneratedThumbnailHeight = 60;
                        // ???
                        System.Diagnostics.Debug.WriteLine("aaaaaaaaaaaa encoder = {0}", encoder);

                        Stream pixelStream = currentOutputImage.PixelBuffer.AsStream();

                        System.Diagnostics.Debug.WriteLine("bbbbbbbbbbbb pixelStream = {0}", pixelStream);
                        byte[] pixels = new byte[pixelStream.Length];


                        //// temporary
                        //for (var i = 0; i < pixels.Length; i++) {
                        //    //if(pixels[i] == 0) {   // white is background
                        //    pixels[i] = 19;
                        //    //}
                        //}
                        //// temporary

                        await pixelStream.ReadAsync(pixels, 0, pixels.Length);
                        System.Diagnostics.Debug.WriteLine("cccccccccccc");
                        encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                                  (uint) currentOutputImage.PixelWidth,
                                  (uint) currentOutputImage.PixelHeight,
                                  96.0,
                                  96.0,
                                  pixels);
                        System.Diagnostics.Debug.WriteLine("dddddddddddd");
                        await encoder.FlushAsync();
                    }

                }
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            TextBoxInputContent.Text = "";
            ButtonGenerate.IsEnabled = false;
            ButtonSaveImage.IsEnabled = false;
            currentOutputImage = null;
            ImageQrCodeImage.Source = null;
        }

        private async void ButtonGenerate_Click(object sender, RoutedEventArgs e)
        {
            var content = TextBoxInputContent.Text;
            if (!String.IsNullOrEmpty(content)) {

                // Default or User-defined values.
                var bmpWidth = QrCodeOptionsDataManager.Instance.Width;
                var bmpHeight = QrCodeOptionsDataManager.Instance.Height;
                // var bmpMargin = (int) (Math.Min(bmpWidth, bmpHeight) / 200) + 1;
                var bmpMargin = QrCodeOptionsDataManager.Instance.Margin;
                var bgColor = QrCodeOptionsDataManager.Instance.BackgroundColor;

                var bitmap = await QrEncodeHelper.Instance.GenerateQrCodeImageAsync(content, bgColor, bmpWidth, bmpHeight, bmpMargin);
                if (bitmap != null) {
                    System.Diagnostics.Debug.WriteLine("Generated QR code image. Size: {0}x{1}", bitmap.PixelWidth, bitmap.PixelHeight);
                    InstantToastHelper.Instance.ShowToast("Successfully generated QR code image.");


                    System.Diagnostics.Debug.WriteLine("PixelBuffer.Length = {0}", bitmap.PixelBuffer.Length);
                    System.Diagnostics.Debug.WriteLine("PixelBuffer = {0}", bitmap.PixelBuffer.ToString());

                    //if (bitmap.PixelBuffer != null) {
                    //    var arr = bitmap.PixelBuffer.ToArray();  // null argument exception ????
                    //    for (var i = 0; i < arr.Length; i++) {
                    //        if (i % 100 == 0) {
                    //            System.Diagnostics.Debug.WriteLine("i: {0}, byte: {1}", i, arr[i]);
                    //        }
                    //    }
                    //}





                    currentOutputImage = bitmap;

                    // TBD:
                    ImageQrCodeImage.Source = currentOutputImage;
                    //ImageQrCodeImage.Width = currentOutputImage.PixelWidth;
                    //ImageQrCodeImage.Height = currentOutputImage.PixelHeight;
                    //ImageQrCodeImage.InvalidateMeasure();
                    //ImageQrCodeImage.InvalidateArrange();
                    // ????


                    ButtonSaveImage.IsEnabled = true;
                } else {
                    System.Diagnostics.Debug.WriteLine("Failed to generate QR code image.");
                    InstantToastHelper.Instance.ShowToast("Failed to generate QR code image.");

                    ImageQrCodeImage.Source = null;
                    ButtonSaveImage.IsEnabled = false;

                }
            }
        }

        private void TextBoxInputContent_TextChanged(object sender, TextChangedEventArgs e)
        {
            var content = TextBoxInputContent.Text;
            if (!String.IsNullOrEmpty(content)) {
                ButtonGenerate.IsEnabled = true;
            } else {
                ButtonGenerate.IsEnabled = false;
            }
        }

        private void ButtonQrCodeOptions_Click(object sender, RoutedEventArgs e)
        {
            if (PopupQrCodeEncodeOptions.IsOpen == false) {
                PopupQrCodeEncodeOptions.HorizontalOffset = 10;
                PopupQrCodeEncodeOptions.VerticalOffset = 20;
                PopupQrCodeEncodeOptions.IsOpen = true;
            }
        }
    }
}
