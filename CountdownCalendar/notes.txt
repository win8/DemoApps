

[ Windows Store App submission checklist ]
http://msdn.microsoft.com/en-us/library/windows/apps/hh694062.aspx


## Description 

A countdown clock/calendar. This app displays a count-down timer to a specified event.

This is a demo app from a series of "training apps" we are building to practice Windows app development.

Note that this app is intended to be used in the "Snap View" (docked on the left- or right-hand side of the screen with a narrow width (typically, 320~500 pixels)).


## Screenshots

1366 x 768

Countdown clock widget.



## App Features

## Keywords

Digital clock
Windows universal app development
Windows 8.1 Phone app development


## Copyright & Trademark

Copyright (c) 2015, Holo Software



## App website

http://www.windowsdemoapps.com/

## Support contact info

http://www.windowsdemoapps.com/support
holosoftware@outlook.com


## Privacy policy

http://www.windowsdemoapps.com/privacy


## Legal

http://www.windowsdemoapps.com/legal



