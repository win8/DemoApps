﻿using StopWatch.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace StopWatch
{
    public sealed partial class StopWatchControl : UserControl
    {
        private BasicStopWatch basicStopWatch;

        public StopWatchControl()
        {
            this.InitializeComponent();

            basicStopWatch = new BasicStopWatch();
            UpdateButtonStates();

            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();
        }
        private void dispatcherTimer_Tick(object sender, object e)
        {
            var strTime = basicStopWatch.ElapsedMillisFormatted;

            TextBlockTime1.Text = strTime;
        }

        private void UpdateButtonStates()
        {
            var watchStatus = basicStopWatch.Status;
            switch (watchStatus) {
                case StopWatchStatus.UNSET:
                default:
                    ButtonStopWatchReset.IsEnabled = true;
                    ButtonStopWatchStart.IsEnabled = false;
                    ButtonStopWatchPause.IsEnabled = false;
                    ButtonStopWatchStop.IsEnabled = false;
                    break;
                case StopWatchStatus.INITIALIZED:
                    ButtonStopWatchReset.IsEnabled = false;
                    ButtonStopWatchStart.IsEnabled = true;
                    ButtonStopWatchPause.IsEnabled = false;
                    ButtonStopWatchStop.IsEnabled = false;
                    break;
                case StopWatchStatus.RUNNING:
                    ButtonStopWatchReset.IsEnabled = false;
                    ButtonStopWatchStart.IsEnabled = false;
                    ButtonStopWatchPause.IsEnabled = true;
                    ButtonStopWatchStop.IsEnabled = true;
                    break;
                case StopWatchStatus.PAUSED:
                    ButtonStopWatchReset.IsEnabled = false;
                    ButtonStopWatchStart.IsEnabled = true;
                    ButtonStopWatchPause.IsEnabled = false;
                    ButtonStopWatchStop.IsEnabled = true;
                    break;
                case StopWatchStatus.STOPPED:
                    ButtonStopWatchReset.IsEnabled = true;
                    ButtonStopWatchStart.IsEnabled = false;
                    ButtonStopWatchPause.IsEnabled = false;
                    ButtonStopWatchStop.IsEnabled = false;
                    break;

            }

        }

        private void ButtonStopWatchStart_Click(object sender, RoutedEventArgs e)
        {
            basicStopWatch.Start();
            UpdateButtonStates();

        }

        private void ButtonStopWatchPause_Click(object sender, RoutedEventArgs e)
        {
            basicStopWatch.Pause();
            UpdateButtonStates();

        }

        private void ButtonStopWatchStop_Click(object sender, RoutedEventArgs e)
        {
            basicStopWatch.Stop();
            UpdateButtonStates();

        }

        private void ButtonStopWatchReset_Click(object sender, RoutedEventArgs e)
        {
            basicStopWatch.Reset();
            UpdateButtonStates();

        }

    }
}
