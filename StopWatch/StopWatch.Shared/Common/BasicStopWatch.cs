﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace StopWatch.Common
{
    public enum StopWatchStatus
    {
        UNSET = 0,
        INITIALIZED,
        RUNNING,
        PAUSED,
        STOPPED
    }

    public class BasicStopWatch
    {
        //// temporary
        //private static readonly ulong DEFAULT_MILLIS = 50UL;

        // Cumulative run times in milliseconds.
        private ulong elapsedMillis;
        // private ulong totalMillis;

        // Unix epoch time millis.
        //private ulong startedTime;
        private long startedTime;
        private long stoppedTime;

        // ..
        private StopWatchStatus status;

        // Elapsed time since startedTime -> unix epoch time
        private SortedDictionary<ulong, long> lapTimes;
        private SortedDictionary<ulong, long> pausedTimes;
        private SortedDictionary<ulong, long> startedTimes;

        public BasicStopWatch()
        {
            ResetCounters();
            // status = StopWatchStatus.UNSET;
            status = StopWatchStatus.INITIALIZED;
        }
        private void ResetCounters()
        {
            elapsedMillis = 0UL;
            startedTime = 0L;
            stoppedTime = 0L;
            lapTimes = new SortedDictionary<ulong, long>();
            pausedTimes = new SortedDictionary<ulong, long>();
            startedTimes = new SortedDictionary<ulong, long>();
        }

        private void UpdateElapsedMillis()
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            UpdateElapsedMillis(now);
        }
        private void UpdateElapsedMillis(long now)
        {
            if (status == StopWatchStatus.RUNNING) {
                var elapsed = 0UL;
                var sl0 = now;
                foreach(KeyValuePair<ulong, long> pair in startedTimes.Reverse()) {
                    ulong su = pair.Key;
                    long sl = pair.Value;
                    elapsed += (ulong) (sl0 - sl);
                    if (pausedTimes.ContainsKey(su)) {
                        sl0 = pausedTimes[su];
                    } else {
                        // first started time. Or, the last item in iteration.
                        break;  // Just to be sure...
                    }
                }
                elapsedMillis = elapsed;
            }
        }


        public ulong ElapsedMillis
        {
            get
            {
                return elapsedMillis;
            }
        }

        // TBD:
        // Format example: http://msdn.microsoft.com/en-us/library/windows/apps/dd992632.aspx
        // ...
        
        public string ElapsedMillisFormatted
        {
            get
            {
                UpdateElapsedMillis();
                TimeSpan time = elapsedMillis.ToTimeSpan();
                // var strTime = time.ToString("c");

                // Heck
                //   (when the last digit ends with "0", the "g" format does not display it) 
                var strTime = time.ToString("g");
                strTime = reformatTimeString(strTime);
                // Heck

                return strTime;
            }
        }

        private string reformatTimeString(string strTime)
        {
            // Heck
            var idx = strTime.LastIndexOf(".");
            if (idx == -1) {
                strTime = strTime + ".000";
            } else if (idx != strTime.Length - 4) {
                if (idx == strTime.Length - 3) {
                    strTime += "0";
                } else if (idx == strTime.Length - 2) {
                    strTime += "00";
                } else if (idx == strTime.Length - 1) {
                    strTime += "000";
                }
            }
            // Heck

            return strTime;
        }

        
        
        public long StartedTime
        {
            get
            {
                return startedTime;
            }
        }
        public DateTime StartedDateTime
        {
            get
            {
                return startedTime.ToLocalDateTime();
            }
        }

        public long StoppedTime
        {
            get
            {
                return stoppedTime;
            }
        }
        public DateTime StoppedDateTime
        {
            get
            {
                return stoppedTime.ToLocalDateTime();
            }
        }


        public ulong TotalDuration
        {
            get
            {
                if (status == StopWatchStatus.RUNNING
                    || status == StopWatchStatus.PAUSED
                    || status == StopWatchStatus.STOPPED) {
                    var endTime = DateTimeUtil.CurrentUnixEpochMillis();
                    if (status == StopWatchStatus.STOPPED) {
                        endTime = stoppedTime;
                    }
                    var duration = (ulong) (endTime - startedTime);
                    return duration;
                } else {
                    return 0UL;
                }
            }
        }


        public StopWatchStatus Status
        {
            get
            {
                return status;
            }
        }



        public List<ulong> GetLapTimes(int count)
        {
            List<ulong> keys = new List<ulong>(lapTimes.Keys);

            var size = keys.Count;
            if(size > count) {
                keys = keys.GetRange((size - count), count);
            }

            keys.Reverse();   // Most recent laptimes first.
            return keys;
        }
        public List<string> GetLapTimesFormatted(int count)
        {
            List<ulong> keys = GetLapTimes(count);

            var times = new List<string>();
            foreach(var k in keys) {
                TimeSpan time = k.ToTimeSpan();
                // Heck
                //   (when the last digit ends with "0", the "g" format does not display it) 
                var strTime = time.ToString("g");
                strTime = reformatTimeString(strTime);
                // Heck
                times.Add(strTime);
            }

            return times;
        }




        //// ????
        //internal ulong MoveForward()
        //{
        //    return MoveForward(DEFAULT_MILLIS);
        //}
        //internal ulong MoveForward(ulong millis)
        //{
        //    if (status == StopWatchStatus.RUNNING) {
        //        elapsedMillis += millis;
        //        return elapsedMillis;
        //    } else {
        //        throw new InvalidOperationException("Stop watch is not running.");
        //    }
        //}


        public void Reset()
        {
            ResetCounters();
            status = StopWatchStatus.INITIALIZED;
        }

        // Start or restart.
        public long Start()
        {
            if (status == StopWatchStatus.INITIALIZED || status == StopWatchStatus.PAUSED) {
                long now = DateTimeUtil.CurrentUnixEpochMillis();
                if (status == StopWatchStatus.INITIALIZED) {
                    startedTime = now;
                } else {
                    // startedTimes.Add(elapsedMillis, now);
                }
                // No need.
                // elapsed time is either zero,
                // or it has been updated at the last Pause.
                // UpdateElapsedMillis(now);
                // --> Actually, this should NOT be called to keep startedTimes and pausedTimes in sync.
                // ...
                startedTimes.Add(elapsedMillis, now);
                status = StopWatchStatus.RUNNING;

                // TBD: Convert to DateTime?
                return now;
            } else {
                throw new InvalidOperationException("Stop watch is not initialized or paused.");
            }
        }

        public ulong Lap()
        {
            if (status == StopWatchStatus.RUNNING) {

                // TBD: Update elapsedMillis first ???
                long now = DateTimeUtil.CurrentUnixEpochMillis();
                UpdateElapsedMillis(now);
                lapTimes.Add(elapsedMillis, now);

                // TBD: Convert to TimeSpan?
                return elapsedMillis;
            } else {
                throw new InvalidOperationException("Stop watch is not running.");
            }
        }

        public ulong Pause()
        {
            if (status == StopWatchStatus.RUNNING) {
                
                // TBD: Update elapsedMillis first ???
                long now = DateTimeUtil.CurrentUnixEpochMillis();
                UpdateElapsedMillis(now);
                pausedTimes.Add(elapsedMillis, now);

                status = StopWatchStatus.PAUSED;

                // TBD: Convert to DateTime?
                // return now;
                // TBD: Convert to TimeSpan?
                return elapsedMillis;
            } else {
                throw new InvalidOperationException("Stop watch is not running.");
            }
        }

        public ulong Stop()
        {
            if (status == StopWatchStatus.RUNNING || status == StopWatchStatus.PAUSED) {

                var now = DateTimeUtil.CurrentUnixEpochMillis();
                stoppedTime = now;
                UpdateElapsedMillis(now);
                status = StopWatchStatus.STOPPED;

                // TBD: Convert to DateTime?
                // return now;
                // TBD: Convert to TimeSpan?
                return elapsedMillis;
            } else {
                throw new InvalidOperationException("Stop watch is not running/paused.");
            }
        }


    }

}
