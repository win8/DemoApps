﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace StopWatch.Common
{

    public sealed class ToastNotificationHelper
    {
        private static ToastNotificationHelper instance = null;
        public static ToastNotificationHelper Instance
        {
            get 
            {
                if (instance == null) {
                    instance = new ToastNotificationHelper();
                }
                return instance;
            }
        }
        private ToastNotificationHelper()
        {
        }

        public void ShowToast(string message)
        {
            ShowInstantToastMessage(message);
        }
        private void ShowInstantToastMessage(string message)
        {
            try {
                ToastTemplateType toastTemplate = ToastTemplateType.ToastText01;
                XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
                XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");
                toastTextElements[0].AppendChild(toastXml.CreateTextNode(message));

                var toast = new ToastNotification(toastXml);
                ToastNotificationManager.CreateToastNotifier().Show(toast);
            } catch (Exception) {
                // Ignore
            }
        }

    }
}
