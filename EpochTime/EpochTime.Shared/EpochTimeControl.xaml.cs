﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using EpochTime.Common;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace EpochTime
{
    public sealed partial class EpochTimeControl : UserControl
    {
        private long _timeMillis = 0L;

        public EpochTimeControl()
        {
            this.InitializeComponent();

#if WINDOWS_APP
            StackPanelDatePicker.Visibility = Visibility.Visible;
            DatePickerDate.Visibility = Visibility.Collapsed;
            StackPanelTimePicker.Visibility = Visibility.Visible;
            TimePickerTime.Visibility = Visibility.Collapsed;
#elif WINDOWS_PHONE_APP
                StackPanelDatePicker.Visibility = Visibility.Collapsed;
                DatePickerDate.Visibility = Visibility.Visible;
                StackPanelTimePicker.Visibility = Visibility.Collapsed;
                TimePickerTime.Visibility = Visibility.Visible;
#else
                StackPanelDatePicker.Visibility = Visibility.Collapsed;
                DatePickerDate.Visibility = Visibility.Collapsed;
                StackPanelTimePicker.Visibility = Visibility.Collapsed;
                TimePickerTime.Visibility = Visibility.Collapsed;
#endif

            if (ButtonCopy != null) {
                ButtonCopy.IsEnabled = false;
#if WINDOWS_APP
                // Clipboard API supported only on Windows.
                ButtonCopy.Visibility = Visibility.Visible;
#else
// #if WINDOWS_PHONE_APP
            // Hide the copy-to-clipboard button on Windows Phone since it's not supported
            ButtonCopy.Visibility = Visibility.Collapsed;            
#endif
            }
        }

        public long TimeInMillis
        {
            get
            {
                return _timeMillis;
            }
            set
            {
                _timeMillis = value;
                TextBoxEpochTime.Text = _timeMillis.ToString();

                // ????
                var strDate = "";
                var strTime = "";
                if (_timeMillis > 0L) {
                    // var dateTime = new DateTime(_timeMillis);
                    var dateTime = _timeMillis.ToLocalDateTime();
                    var strDateTime = dateTime.ToString();
                    var parts = strDateTime.Split(new char[] { ' ' }, 2);
                    if (parts != null && parts.Length > 0) {
                        strDate = parts[0];
                        if (parts.Length > 1) {
                            strTime = parts[1];
                        }
                    }
                }
#if WINDOWS_APP
                TextBoxDatePart.Text = strDate;
                TextBoxTimePart.Text = strTime;
#else
                try {
                    DatePickerDate.Date = DateTime.Parse(strDate);
                } catch (Exception ex) {
                    // What to do???
                    System.Diagnostics.Debug.WriteLine("Failed to parse date/time: strDate = " + strDate, ex);
                }
                try {
                    // TimePickerTime.Time = DateTime.Parse(strTime);
                    // heck!!!
                    // TimePickerTime.Time = DateTime.Parse(strTime) - DateTime.Parse("0:00:00 AM");    // ?????
                    var ts = DateTime.Parse(strTime);
                    TimePickerTime.Time = new TimeSpan(ts.Hour, ts.Minute, ts.Second);    // ????
                } catch (Exception ex) {
                    // What to do???
                    System.Diagnostics.Debug.WriteLine("Failed to parse date/time: strTime = " + strTime, ex);
                }
#endif
            }
        }


        private void RefreshCopyButton()
        {
            if (ButtonCopy != null) {
#if WINDOWS_APP
                var strDate = TextBoxDatePart.Text;
                if (!String.IsNullOrEmpty(strDate)) {
                    ButtonCopy.IsEnabled = true;
                } else {
                    ButtonCopy.IsEnabled = false;
                }
#endif
            }
        }


        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            this.TimeInMillis = 0L;
            //TextBoxEpochTime.Text = "0";
            //TextBoxDatePart.Text = "";
            //TextBoxTimePart.Text = "";

            RefreshCopyButton();
        }

        private void ButtonNow_Click(object sender, RoutedEventArgs e)
        {
            // var now = DateTime.Now.Ticks;
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            this.TimeInMillis = now;

            RefreshCopyButton();
        }

        private void ButtonConvertToDateTime_Click(object sender, RoutedEventArgs e)
        {
            var strTimeMillis = TextBoxEpochTime.Text;
            if (!String.IsNullOrEmpty(strTimeMillis)) {
                try {
                    var t = Convert.ToInt64(strTimeMillis);
                    this.TimeInMillis = t;
                } catch (Exception) {
                    // Ignore
                }
            } else {
                // ????
            }
            RefreshCopyButton();
        }


        // TBD:
        // There is a bug.
        // Sometimes (don't know exactly when/how, but it happens more often than just "sometimes')
        // 12 hours are added to (presumably) the millis,
        // which in turn converts the date time strings.
        //   (e.g., 2/20/2015 2:10 PM becomes 2/21/2015 2:10 AM, and
        //          2/22/2015 2:10 AM becomes 2/22/2015 2:20 PM, etc.)

        private void ButtonConvertToUnixEpoch_Click(object sender, RoutedEventArgs e)
        {
            var milliSecs = 0L;

#if WINDOWS_APP
            var strDate = TextBoxDatePart.Text;
#else
            var strDate = DatePickerDate.Date.ToString();     // ????
#endif
            try {
                var dtDate = DateTime.Parse(strDate);
                if (dtDate != null) {
                    // var d = dtDate.Ticks;
                    var d = dtDate.ToUnixEpochMillis();
                    milliSecs += d;
                }
            } catch (Exception) {
                // Ignore
            }
#if WINDOWS_APP
            var strTime = TextBoxTimePart.Text;
#else
            var strTime = TimePickerTime.Time.ToString();
#endif
            try {
                // TBD: The parse() does not generally work for the format we use, for some reason.
                // var tsTime = TimeSpan.Parse(strTime);
                // heck!!!
                // var tsTime = DateTime.Parse(strTime) - DateTime.Parse("0:00:00 AM");    // ?????
                var ts = DateTime.Parse(strTime);
                var tsTime = new TimeSpan(ts.Hour, ts.Minute, ts.Second);    // ????
                if (tsTime != null) {
                    // var t = tsTime.Ticks / TimeSpan.TicksPerMillisecond;
                    // var t = tsTime.Ticks;
                    var t = tsTime.ToMilliseconds();

                    // ...
                    milliSecs += t;
                }
            } catch (Exception) {
                // Ignore
            }

            this.TimeInMillis = milliSecs;
            RefreshCopyButton();
        }


        private void DatePickerDate_Loaded(object sender, RoutedEventArgs e)
        {
            //var strDate = TextBoxDatePart.Text;
            //if (!String.IsNullOrEmpty(strDate)) {
            //    DatePicker picker = sender as DatePicker;
            //    try {
            //        var dtDate = DateTime.Parse(strDate);
            //        if (dtDate != null) {
            //            picker.Date = dtDate;
            //        }
            //    } catch (Exception) {
            //        // Ignore
            //    }
            //}
        }

        private void TimePickerTime_Loaded(object sender, RoutedEventArgs e)
        {
            //var strTime = TextBoxTimePart.Text;
            //if (!String.IsNullOrEmpty(strTime)) {
            //    TimePicker picker = sender as TimePicker;
            //    try {
            //        // TBD: The parse() does not generally work for the format we use, for some reason.
            //        var tsTime = TimeSpan.Parse(strTime);
            //        if (tsTime != null) {
            //            picker.Time = tsTime;
            //        }
            //    } catch (Exception) {
            //        // Ignore
            //    }
            //}
        }


        private void DatePickerDate_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            //try {
            //    var dateTime = DatePickerOne.Date;
            //    if (dateTime != null) {
            //        var strDate = dateTime.ToString();
            //        if (strDate != null) {
            //            TextBoxDatePart.Text = strDate;
            //        }
            //    }
            //} catch (Exception) {
            //    // Ignore
            //}
        }

        private void TimePickerTime_TimeChanged(object sender, TimePickerValueChangedEventArgs e)
        {
            //try {
            //    var timeSpan = TimePickerOne.Time;
            //    if (timeSpan != null) {
            //        var strTime = timeSpan.ToString();
            //        if (strTime != null) {
            //            TextBoxTimePart.Text = strTime;
            //        }
            //    }
            //} catch (Exception) {
            //    // Ignore
            //}
        }


        private void DatePickerOne_Loaded(object sender, RoutedEventArgs e)
        {
            var strDate = TextBoxDatePart.Text;
            if (!String.IsNullOrEmpty(strDate)) {
                DatePicker picker = sender as DatePicker;
                try {
                    var dtDate = DateTime.Parse(strDate);
                    if (dtDate != null) {
                        picker.Date = dtDate;
                    }
                } catch (Exception) {
                    // Ignore
                }
            }
        }

        private void TimePickerOne_Loaded(object sender, RoutedEventArgs e)
        {
            var strTime = TextBoxTimePart.Text;
            if (!String.IsNullOrEmpty(strTime)) {
                TimePicker picker = sender as TimePicker;
                try {
                    // TBD: The parse() does not generally work for the format we use, for some reason.
                    var tsTime = TimeSpan.Parse(strTime);
                    if (tsTime != null) {
                        picker.Time = tsTime;
                    }
                } catch (Exception) {
                    // Ignore
                }
            }
        }

        private void ButtonFlyoutDatePickerOK_Click(object sender, RoutedEventArgs e)
        {
            try {
                var dateTime = DatePickerOne.Date;
                if (dateTime != null) {
                    var strDate = dateTime.ToString();
                    if (strDate != null) {
                        TextBoxDatePart.Text = strDate;
                    }
                }
            } catch (Exception) {
                // Ignore
            }

            FlyoutDatePicker.Hide();
        }
        private void ButtonFlyoutDatePickerCancel_Click(object sender, RoutedEventArgs e)
        {
            FlyoutDatePicker.Hide();
        }

        private void ButtonFlyoutTimePickerOK_Click(object sender, RoutedEventArgs e)
        {
            try {
                var timeSpan = TimePickerOne.Time;
                if (timeSpan != null) {
                    var strTime = timeSpan.ToString();
                    if (strTime != null) {
                        TextBoxTimePart.Text = strTime;
                    }
                }
            } catch (Exception) {
                // Ignore
            }

            FlyoutTimePicker.Hide();
        }
        private void ButtonFlyoutTimePickerCancel_Click(object sender, RoutedEventArgs e)
        {
            FlyoutTimePicker.Hide();
        }




        private void ButtonCopy_Click(object sender, RoutedEventArgs e)
        {
            var strDate = TextBoxDatePart.Text;
            var strTime = TextBoxTimePart.Text;

            var strDateTime = "";
            if (!String.IsNullOrEmpty(strDate)) {
                strDateTime = strDate;
            }
            if (!String.IsNullOrEmpty(strTime)) {
                if (!String.IsNullOrEmpty(strDateTime)) {
                    strDateTime += " " + strTime;
                } else {
                    strDateTime = strTime;
                }
            }

            if (!String.IsNullOrEmpty(strDateTime)) {
                if (ButtonCopy != null) {
#if WINDOWS_APP
                    Windows.ApplicationModel.DataTransfer.DataPackage dataPackage = new Windows.ApplicationModel.DataTransfer.DataPackage();
                    dataPackage.SetText(strDateTime);
                    Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(dataPackage);
#else
// #elif WINDOWS_PHONE_APP
            // ???
#endif
                }
            }

        }


    }
}
