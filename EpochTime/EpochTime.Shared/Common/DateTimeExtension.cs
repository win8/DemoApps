﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpochTime.Common
{
    public static class DateTimeExtension
    {
        // Note: All DateTime args are based on local time.

        // "Epoch".
        private static readonly DateTime EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime ToLocalDateTime(this long epochMillis)
        {
            // return epochMillis.ToLocalDateTime(0L);
            return EPOCH.AddMilliseconds(epochMillis).ToLocalTime();
        }
        public static DateTime ToLocalDateTime(this long epochMillis, long delta)
        {
            return EPOCH.AddMilliseconds(epochMillis + delta).ToLocalTime();
        }

        //public static DateTime? ToLocalDateTime(this long epochMillis)
        //{
        //    // return EPOCH.AddMilliseconds(epochMillis).ToLocalTime();
        //    return epochMillis.ToLocalDateTime(0L);
        //}
        //public static DateTime? ToLocalDateTime(this long epochMillis, long delta)
        //{
        //    DateTime? dateTime = null;
        //    try {
        //        dateTime = EPOCH.AddMilliseconds(epochMillis + delta).ToLocalTime();
        //    } catch (Exception) {
        //        // Ignore...
        //    }
        //    return dateTime;
        //}

        public static long ToUnixEpochMillis(this DateTime dateTime)
        {
            return Convert.ToInt64((dateTime.ToUniversalTime() - EPOCH).TotalMilliseconds);
        }

        public static long ToMilliseconds(this TimeSpan timeSpan)
        {
            // return timeSpan.TotalMilliseconds;
            return (timeSpan.Ticks / TimeSpan.TicksPerMillisecond);
        }
    }
}
