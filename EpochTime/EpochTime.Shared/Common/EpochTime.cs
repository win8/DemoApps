﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


// Not being used.

// To be deleted
// Unless we use a single textbox for datetime.
// (Currently, we use two separate textboxes for date and time.)
namespace EpochTime.Common
{
    public class EpochTime : INotifyPropertyChanged
    {
        private long _timeMillis = 0L;

        public long TimeInMillis
        {
            get
            {
                return _timeMillis;
            }
            set
            {
                _timeMillis = value;
                RaisePropertyChanged("TimeMillis");
            }
        }

        public string DateTimeString
        {
            get
            {
                // var dateTime = new DateTime(_timeMillis);
                var dateTime = _timeMillis.ToLocalDateTime();
                var strDate = dateTime.ToString();
                return strDate;
            }
            set
            {
                try {
                    var strDate = DateTime.Parse(value);
                    if (strDate != null) {
                        // _timeMillis = strDate.Ticks;    // ????
                        _timeMillis = strDate.ToUnixEpochMillis();
                        RaisePropertyChanged("TimeMillis");
                    } else {
                        // ????
                    }
                } catch (Exception) {
                    // Ignore.
                }
            }
        }

        public string DateString
        {
            get
            {
                // var dateTime = new DateTime(_timeMillis);
                var dateTime = _timeMillis.ToLocalDateTime();
                var strDateTime = dateTime.ToString();
                var parts = strDateTime.Split(new char[]{' '}, 2);
                var strDate = "";
                if (parts != null && parts.Length > 0) {
                    strDate = parts[0];
                }
                return strDate;
            }
            set
            {
                try {
                    var strDate = DateTime.Parse(value);
                    // var strTime = TimeSpan.Parse();

                    if (strDate != null) {
                        // _timeMillis = strDate.Ticks;    // ????
                        _timeMillis = strDate.ToUnixEpochMillis();
                        RaisePropertyChanged("TimeMillis");
                    } else {
                        // ????
                    }
                } catch (Exception) {
                    // Ignore.
                }
            }
        }


    
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

}
