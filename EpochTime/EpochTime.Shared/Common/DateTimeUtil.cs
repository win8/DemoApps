﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EpochTime.Common
{
    public static class DateTimeUtil
    {
        public static long CurrentUnixEpochMillis()
        {
            return DateTime.Now.ToUnixEpochMillis();
        }
    }
}
