﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MultiClipboard
{
    public sealed partial class MultiClipboardControl : UserControl
    {
        // TBD:
        // Note that we use a bit strange implementation.
        // The four textbox fields and the clip stack are used together.
        // The first element of clips is sort of the fifth text box, etc... 
        private Stack<string> clips = new Stack<string>();

        public MultiClipboardControl()
        {
            this.InitializeComponent();
        }

        private void ButtonClipRetrieve_Click(object sender, RoutedEventArgs e)
        {
            string strClipA = TextBoxClipA.Text;
#if WINDOWS_APP
            if (!String.IsNullOrEmpty(strClipA)) {
                Windows.ApplicationModel.DataTransfer.DataPackage dataPackage = new Windows.ApplicationModel.DataTransfer.DataPackage();
                dataPackage.SetText(strClipA);
                Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(dataPackage);
            }
#elif WINDOWS_PHONE_APP
            // ....
#endif
            TextBoxClipA.Text = TextBoxClipB.Text;
            TextBoxClipB.Text = TextBoxClipC.Text;
            TextBoxClipC.Text = TextBoxClipD.Text;
            string top = null;
            if (clips.Count > 0) {
                try {
                    top = clips.Pop();
                } catch (Exception) {
                    // Ignore
                }
            }
            if (top == null) {
                top = "";
            }
            TextBoxClipD.Text = top;
        }

#if WINDOWS_APP
        private async void ButtonClipStore_Click(object sender, RoutedEventArgs e)
        {
            Windows.ApplicationModel.DataTransfer.DataPackageView dataPackageView = Windows.ApplicationModel.DataTransfer.Clipboard.GetContent();
            if (dataPackageView != null) {
                string strClipA = await dataPackageView.GetTextAsync();
                if (!String.IsNullOrEmpty(strClipA)) {
                    string currentVal = TextBoxClipA.Text;
                    if (String.IsNullOrEmpty(currentVal) || !currentVal.Equals(strClipA)) {   // Do not copy the same value again.
                        if (!String.IsNullOrEmpty(currentVal)) {
                            string top = TextBoxClipD.Text;
                            if (!String.IsNullOrEmpty(top)) {
                                clips.Push(top);
                            }
                            TextBoxClipD.Text = TextBoxClipC.Text;
                            TextBoxClipC.Text = TextBoxClipB.Text;
                            TextBoxClipB.Text = TextBoxClipA.Text;
                        }
                        TextBoxClipA.Text = strClipA;
                    }
                }
            }
        }
#elif WINDOWS_PHONE_APP
        private void ButtonClipStore_Click(object sender, RoutedEventArgs e)
        {
            string strClipA = TextBoxClipA.Text;
            if (!String.IsNullOrEmpty(strClipA)) {
                string top = TextBoxClipD.Text;
                if (!String.IsNullOrEmpty(top)) {
                    clips.Push(top);
                }
                TextBoxClipD.Text = TextBoxClipC.Text;
                TextBoxClipC.Text = TextBoxClipB.Text;
                TextBoxClipB.Text = strClipA;
            }
            TextBoxClipA.Text = "";
        }
#endif

        private void ButtonClipReset_Click(object sender, RoutedEventArgs e)
        {
            clips.Clear();
            TextBoxClipA.Text = "";
            TextBoxClipB.Text = "";
            TextBoxClipC.Text = "";
            TextBoxClipD.Text = "";
        }
    }

}
