﻿using DemoCore.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace QuickEmail
{
    public sealed partial class QuickEmailControl : UserControl
    {
        public QuickEmailControl()
        {
            this.InitializeComponent();

            ButtonStartSending.IsEnabled = false;
        }

        private void ButtonStartSending_Click(object sender, RoutedEventArgs e)
        {
            var subject = TextBoxEmailSubject.Text;
            var msgBody = TextBoxEmailBody.Text;

            // temporary
            //if (String.IsNullOrEmpty(subject)) {
            //    subject = "Re: ";
            //}
            if (String.IsNullOrEmpty(msgBody)) {
                msgBody = "...";
            }
            // temporary

            if (!String.IsNullOrEmpty(subject) || !String.IsNullOrEmpty(msgBody)) {
                // draftMessage = MessageDataHelper.Instance.StoreDraftMessage(draftMessage.Value);
                var emailShareHandler = new EmailShareHandler(subject, msgBody);
                emailShareHandler.InitiateShare(ShareFormat.Text);

                // Explicit
                Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
            }
        }

        private void TextBoxEmailSubject_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Subject can be empty...
        }

        private void TextBoxEmailBody_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Message body cannot be empty.
            var msgBody = TextBoxEmailBody.Text;
            if (String.IsNullOrEmpty(msgBody)) {
                ButtonStartSending.IsEnabled = false;
            } else {
                ButtonStartSending.IsEnabled = true;
            }
        }

    }
}
