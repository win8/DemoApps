﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileCrypt.Pickers
{
    // These are used only for WP81
    // but, they may be used in the common method signatures, etc...
    // ...

    public enum FolderPickerContext : short
    {
        EncryptedFileFolder = 1,
        DecryptedFileFolder,
    }
    public enum FileOpenPickerContext : short
    {
        EncryptedInputFile = 1,
        DecryptedInputFile,
    }
    public enum FileSavePickerConstext : short
    {
        EncryptedOutputFile = 1,
        DecryptedOutputFile,
    }

    public static class PickerConstants
    {
        // Better names???
        public const string KEY_CONTEXT_FOR_FOLDER_PICKER = "folderContext";
        public const string KEY_CONTEXT_FOR_FILE_OPEN_PICKER = "fileOpenContext";
        public const string KEY_CONTEXT_FOR_FILE_SAVE_PICKER = "fileSaveContext";

    }
}
