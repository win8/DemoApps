﻿using FileCrypt.Pickers;
using HoloBase.Crypto;
using HoloBase.Toast;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Cryptography;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FileCrypt
{
    public sealed partial class FileCryptControl : UserControl
    {
        // temporary
        private static readonly string dataProviderDescriptor = "LOCAL=user";
        private static readonly BinaryStringEncoding binaryStringEncoding = BinaryStringEncoding.Utf8;

        // temporary
        private static readonly string plainTextFileOpenPickerSettingsIdentifier = "plainTextFileOpenPicker";
        private static readonly string cypherTextFileOpenPickerSettingsIdentifier = "cypherTextFileOpenPicker";
        private static readonly string encryptedFileFolderPickerSettingsIdentifier = "encryptedFileFolderPicker";
        private static readonly string decryptedFileFolderPickerSettingsIdentifier = "decryptedFileFolderPicker";
        private static readonly string encryptedFileSavePickerSettingsIdentifier = "encryptedFileSavePicker";
        private static readonly string decryptedFileSavePickerSettingsIdentifier = "decryptedFileSavePicker";

        // temporary
        private PickerLocationId defaultInputEncryptedFileFolderId;
        private PickerLocationId defaultInputDecryptedFileFolderId;
        private IStorageFolder defaultOutputEncryptedFileFolder;
        private IStorageFolder defaultOutputDecryptedFileFolder;
        // private string outputFilePath;
        private IStorageFile outputFile;

        public FileCryptControl()
        {
            this.InitializeComponent();

            // temporary
            defaultInputEncryptedFileFolderId = PickerLocationId.ComputerFolder;
            defaultInputDecryptedFileFolderId = PickerLocationId.ComputerFolder;

            // temporary
            //#if WINDOWS_PHONE_APP
            //            defaultOutputEncryptedFileFolder = KnownFolders.RemovableDevices;
            //            defaultOutputDecryptedFileFolder = KnownFolders.RemovableDevices;
            //#else
            //            defaultOutputEncryptedFileFolder = KnownFolders.DocumentsLibrary;
            //            defaultOutputDecryptedFileFolder = KnownFolders.DocumentsLibrary;
            //#endif
            //defaultOutputEncryptedFileFolder = null;
            //defaultOutputDecryptedFileFolder = null;
            //TextBoxEncryptedFileFolder.Text = "";
            //TextBoxDecryptedFileFolder.Text = "";

            defaultOutputEncryptedFileFolder = ApplicationData.Current.TemporaryFolder;
            defaultOutputDecryptedFileFolder = ApplicationData.Current.TemporaryFolder;
            TextBoxEncryptedFileFolder.Text = defaultOutputEncryptedFileFolder.Name;     // Name vs. DisplayName????
            TextBoxDecryptedFileFolder.Text = defaultOutputDecryptedFileFolder.Name;


            // temporary
            // ButtonChangeEncryptFolder.IsEnabled = false;
            // ButtonChangeDecryptFolder.IsEnabled = false;

            // temporary
            //outputFilePath = null;
            outputFile = null;
            TextBoxOutputFilePath.Text = "";
            ButtonOpenOutputFile.IsEnabled = false;

            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
            // ...
        
        }

        // TBD:
        // This check may be required on Windows 8 but not one Windows 10.
        // Need to verify this....
        //private bool EnsureUnsnapped()
        //{
        //    // FilePicker APIs will not work if the application is in a snapped state.
        //    // If an app wants to show a FilePicker while snapped, it must attempt to unsnap first
        //    bool unsnapped = ((ApplicationView.Value != ApplicationViewState.Snapped) || ApplicationView.TryUnsnap());
        //    if (!unsnapped) {
        //        InstantToastHelper.Instance.ShowToast("Cannot unsnap the app.");
        //    }
        //    return unsnapped;
        //}



        private async Task<IStorageFile> ChooseInputFileAsync(string settingsIdentifier, PickerLocationId suggestedStartLocation, FileOpenPickerContext pickerContext)
        {
            IStorageFile file = null;
            // if (EnsureUnsnapped()) {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = suggestedStartLocation;
            openPicker.FileTypeFilter.Add(".txt");
            // openPicker.FileTypeFilter.Add(".crypt");
            openPicker.SettingsIdentifier = settingsIdentifier;
#if WINDOWS_PHONE_APP
            // Note: Enum type cannot be used for ContinuationData.
            openPicker.ContinuationData.Add(PickerConstants.KEY_CONTEXT_FOR_FILE_OPEN_PICKER, (short) pickerContext);
            openPicker.PickSingleFileAndContinue();
            // TBD: Picked file should be handled in OnActivated(IActivatedEventArgs args)
#else
            file = await openPicker.PickSingleFileAsync();
#endif
            // }
            return file;
        }


        private async Task<IStorageFolder> ChooseFolderAsync(string settingsIdentifier, FolderPickerContext pickerContext)
        {
            IStorageFolder folder = null;
            // if (EnsureUnsnapped()) {
            FolderPicker folderPicker = new FolderPicker();
            folderPicker.ViewMode = PickerViewMode.Thumbnail;
            folderPicker.FileTypeFilter.Add(".txt");   // ????
            folderPicker.SettingsIdentifier = settingsIdentifier;
#if WINDOWS_PHONE_APP
            // Note: Enum type cannot be used for ContinuationData.
            folderPicker.ContinuationData.Add(PickerConstants.KEY_CONTEXT_FOR_FOLDER_PICKER, (short) pickerContext);
            folderPicker.PickFolderAndContinue();
            // TBD: Picked folder should be handled in OnActivated(IActivatedEventArgs args)
#else
            folder = await folderPicker.PickSingleFolderAsync();
#endif
            // }
            return folder;
        }


        private async Task<IStorageFile> ChooseOutputFileAsync(string settingsIdentifier, string suggestedFileName, FileSavePickerConstext pickerContext)
        {
            IStorageFile file = null;
            // if (EnsureUnsnapped()) {
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.DefaultFileExtension = ".txt";
            savePicker.FileTypeChoices.Add("Text Files", new List<string> { ".txt" });
            savePicker.SuggestedFileName = suggestedFileName;   // tbd
            savePicker.SettingsIdentifier = settingsIdentifier;

#if WINDOWS_PHONE_APP
            // Note: Enum type cannot be used for ContinuationData.
            savePicker.ContinuationData.Add(PickerConstants.KEY_CONTEXT_FOR_FILE_SAVE_PICKER, (short) pickerContext);
            savePicker.PickSaveFileAndContinue();
            // TBD: Picked file should be handled in OnActivated(IActivatedEventArgs args)
#else
            file = await savePicker.PickSaveFileAsync();
#endif

            // }
            return file;
        }


        // These methods cannot return null or empty string...
        private string GenerateSuggestedOutputFileName(string inputFilePath, string suffix)
        {
            string suggestedOutputFileName = null;
            try {
                // var idx1 = inputFilePath.LastIndexOf(@"\");
                // var inputFileName = (idx1 >= 0) ? ((idx1 < inputFilePath.Length-1) ? inputFilePath.Substring(idx1+1) : "file") : inputFilePath;    // ???
                var inputFileName = Path.GetFileName(inputFilePath);
                var idx2 = inputFileName.LastIndexOf(".");
                var inputFileRoot = (idx2 >= 0) ? inputFileName.Substring(0, idx2) : inputFileName;   // ???
                // var inputFileExt = (idx2 >= 0 && idx2 < inputFileRoot.Length-1) ? inputFileName.Substring(idx2+1) : "";        // ???
                var inputFileExtension = Path.GetExtension(inputFilePath);

                suggestedOutputFileName = inputFileRoot;
                suggestedOutputFileName += "_" + suffix;
                //if (!String.IsNullOrEmpty(inputFileExt)) {
                //    suggestedOutputFileName += "." + inputFileExt;
                //}
                if (!String.IsNullOrEmpty(inputFileExtension) && !inputFileExtension.Equals(".")) {
                    suggestedOutputFileName += inputFileExtension;
                }
            } catch (Exception) {
                // What to do???
                suggestedOutputFileName = "output_file";  // ????
            }
            return suggestedOutputFileName;
        }
        private string GenerateSuggestedEncryptedOutputFileName(string inputFilePath)
        {
            string suggestedOutputFileName = GenerateSuggestedOutputFileName(inputFilePath, "encrypted");
            return suggestedOutputFileName;
        }
        private string GenerateSuggestedDecryptedOutputFileName(string inputFilePath)
        {
            string suggestedOutputFileName = GenerateSuggestedOutputFileName(inputFilePath, "decrypted");
            return suggestedOutputFileName;
        }
        private string GenerateFallbackFileName(string inputFilePath)
        {
            string suggestedOutputFileName = GenerateSuggestedOutputFileName(inputFilePath, "~");
            return suggestedOutputFileName;
        }


        private async Task<IStorageFile> SelectNewEncryptedOutputFileAsync(string inputFileName)
        {
            IStorageFile newOutputFile = null;

            var suggestedOutputFileName = GenerateSuggestedEncryptedOutputFileName(inputFileName);
            if (!String.IsNullOrEmpty(suggestedOutputFileName)) {
                if (defaultOutputEncryptedFileFolder != null) {   // TBD: StorageFolder isValid() ??
                    StorageFile file = null;
                    try {
                        file = await defaultOutputEncryptedFileFolder.GetFileAsync(suggestedOutputFileName);
                    } catch (Exception) {
                        // Ignore.
                    }
                    if (file != null) {
                        // TBD: What to do??
                        // (1) Overwrite?
                        newOutputFile = file;
                        //// (2) Fail?
                        //newOutputFile = null;
                        //// (3) Create a new file? (What if the file with this new name already exists as well????)
                        //var newSuggestOutputFileName = GenerateFallbackFileName(suggestedOutputFileName);
                        //newOutputFile = await defaultOutputEncryptedFileFolder.CreateFileAsync(newSuggestOutputFileName);
                    } else {
                        newOutputFile = await defaultOutputEncryptedFileFolder.CreateFileAsync(suggestedOutputFileName);
                    }
                } else {
                    newOutputFile = await ChooseOutputFileAsync(encryptedFileSavePickerSettingsIdentifier, suggestedOutputFileName, FileSavePickerConstext.EncryptedOutputFile);
                }
            }

            return newOutputFile;
        }

        private async Task<IStorageFile> SelectNewDecryptedOutputFileAsync(string inputFileName)
        {
            IStorageFile newOutputFile = null;

            var suggestedOutputFileName = GenerateSuggestedDecryptedOutputFileName(inputFileName);
            if (!String.IsNullOrEmpty(suggestedOutputFileName)) {
                if (defaultOutputDecryptedFileFolder != null) {   // TBD: StorageFolder isValid() ??
                    StorageFile file = null;
                    try {
                        file = await defaultOutputDecryptedFileFolder.GetFileAsync(suggestedOutputFileName);
                    } catch (Exception) {
                        // Ignore.
                    }
                    if (file != null) {
                        // TBD: What to do??
                        // (1) Overwrite?
                        newOutputFile = file;
                        //// (2) Fail?
                        //newOutputFile = null;
                        //// (3) Create a new file? (What if the file with this new name already exists as well????)
                        //var newSuggestOutputFileName = GenerateFallbackFileName(suggestedOutputFileName);
                        //newOutputFile = await defaultOutputDecryptedFileFolder.CreateFileAsync(newSuggestOutputFileName);
                    } else {
                        newOutputFile = await defaultOutputDecryptedFileFolder.CreateFileAsync(suggestedOutputFileName);
                    }
                } else {
                    newOutputFile = await ChooseOutputFileAsync(decryptedFileSavePickerSettingsIdentifier, suggestedOutputFileName, FileSavePickerConstext.DecryptedOutputFile);
                }
            }

            return newOutputFile;
        }



        private async Task ProtectFileAsync(IStorageFile inputFile, IStorageFile outputFile)
        {
            await DataProtectHelper.Instance.ProtectDataAsync(
                inputFile,
                outputFile,
                dataProviderDescriptor,
                binaryStringEncoding);
        }
        private async Task UnprotectFileAsync(IStorageFile inputFile, IStorageFile outputFile)
        {
            await DataProtectHelper.Instance.UnprotectDataAsync(
                inputFile,
                outputFile,
                binaryStringEncoding);
        }


        private async void ButtonEncrypt_Click(object sender, RoutedEventArgs e)
        {
            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
            // ...
            var inputFile = await ChooseInputFileAsync(plainTextFileOpenPickerSettingsIdentifier, defaultInputDecryptedFileFolderId, FileOpenPickerContext.DecryptedInputFile);
            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
            // ...            
#if WINDOWS_PHONE_APP
#else
            await DoEncryptionUponFilePickAsync(inputFile);
#endif
        }
        public async Task DoEncryptionUponFilePickAsync(IStorageFile inputFile)
        {
            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
            // ...
            if (inputFile != null) {
                var fileName = inputFile.Name;
                TextBoxInputFileName.Text = fileName;

                //outputFilePath = null;
                outputFile = null;
                TextBoxOutputFilePath.Text = "";
                ButtonOpenOutputFile.IsEnabled = false;

                // temporary
                ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                // ...

                IStorageFile newOutputFile = null;
                try {
                    newOutputFile = await SelectNewEncryptedOutputFileAsync(fileName);
                    if (newOutputFile == null) {
                        InstantToastHelper.Instance.ShowToast("Encryption cancelled.");
                        return;
                    }
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("SelectNewEncryptedOutputFile() failed: " + ex.Message);
                    InstantToastHelper.Instance.ShowToast("Encryption failed. Try unsetting or changing the \"Encrypted File Location\".");
                    return;
                }

                // temporary
                ButtonEncrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
                ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                // ...

                try {
                    // "Main" routine.
                    //await EncryptFile(inputFilePath, newOutputFilePath);
                    await ProtectFileAsync(inputFile, newOutputFile);

                    //outputFilePath = newOutputFilePath;
                    //TextBoxOutputFilePath.Text = newOutputFilePath;
                    outputFile = newOutputFile;
                    TextBoxOutputFilePath.Text = newOutputFile.Path;
                    ButtonOpenOutputFile.IsEnabled = true;

                    //// temporary
                    //ButtonEncrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
                    //ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                    //// ...
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("ProtectFile() failed: " + ex.Message);
                    InstantToastHelper.Instance.ShowToast("Encrypt failed.");

                    // temporary
                    ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                    ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                    // ...
                }
            } else {
                // temporary
                ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                // ...            
            }
        }


        private async void ButtonDecrypt_Click(object sender, RoutedEventArgs e)
        {
            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
            // ...
            var inputFile = await ChooseInputFileAsync(cypherTextFileOpenPickerSettingsIdentifier, defaultInputEncryptedFileFolderId, FileOpenPickerContext.EncryptedInputFile);
            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
            // ...            
#if WINDOWS_PHONE_APP
#else
            await DoDecryptionUponFilePickAsync(inputFile);
#endif
        }
        public async Task DoDecryptionUponFilePickAsync(IStorageFile inputFile)
        {
            // temporary
            ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
            ButtonDecrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
            // ...
            if (inputFile != null) {
                var fileName = inputFile.Name;
                TextBoxInputFileName.Text = fileName;

                //outputFilePath = null;
                outputFile = null;
                TextBoxOutputFilePath.Text = "";
                ButtonOpenOutputFile.IsEnabled = false;

                // temporary
                ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                // ...            

                IStorageFile newOutputFile = null;
                try {
                    newOutputFile = await SelectNewDecryptedOutputFileAsync(fileName);
                    if (newOutputFile == null) {
                        InstantToastHelper.Instance.ShowToast("Decryption cancelled.");
                        return;
                    }
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("SelectNewDecryptedOutputFile() failed: " + ex.Message);
                    InstantToastHelper.Instance.ShowToast("Decryption failed. Try unsetting or changing the \"Decrypted File Location\".");
                    return;
                }

                // temporary
                ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                ButtonDecrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
                // ...

                try {
                    // "Main" routine.
                    //await DecryptFile(inputFilePath, newOutputFilePath);
                    await UnprotectFileAsync(inputFile, newOutputFile);

                    //outputFilePath = newOutputFilePath;
                    //TextBoxOutputFilePath.Text = newOutputFilePath;
                    outputFile = newOutputFile;
                    TextBoxOutputFilePath.Text = newOutputFile.Path;
                    ButtonOpenOutputFile.IsEnabled = true;

                    //// temporary
                    //ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                    //ButtonDecrypt.Background = new SolidColorBrush(Colors.DarkSlateBlue);
                    //// ...
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("UnprotectFile() failed: " + ex.Message);
                    InstantToastHelper.Instance.ShowToast("Decrypt failed.");

                    // temporary
                    ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                    ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                    // ...
                }
            } else {
                // temporary
                ButtonEncrypt.Background = new SolidColorBrush(Colors.IndianRed);
                ButtonDecrypt.Background = new SolidColorBrush(Colors.IndianRed);
                // ...            
            }
        }


        private async void ButtonChangeEncryptFolder_Click(object sender, RoutedEventArgs e)
        {
            var folder = await ChooseFolderAsync(encryptedFileFolderPickerSettingsIdentifier, FolderPickerContext.EncryptedFileFolder);
#if WINDOWS_PHONE_APP
#else
            DoChangeEncryptFolder(folder);
#endif
        }
        public void DoChangeEncryptFolder(IStorageFolder folder)
        {
            if (folder != null) {
                defaultOutputEncryptedFileFolder = folder;
                TextBoxEncryptedFileFolder.Text = defaultOutputEncryptedFileFolder.Name;
            } else {
                // ???
            }
        }

        private void ButtonResetEncryptFolder_Click(object sender, RoutedEventArgs e)
        {
            defaultOutputEncryptedFileFolder = null;
            TextBoxEncryptedFileFolder.Text = "";
        }

        private async void ButtonChangeDecryptFolder_Click(object sender, RoutedEventArgs e)
        {
            var folder = await ChooseFolderAsync(decryptedFileFolderPickerSettingsIdentifier, FolderPickerContext.DecryptedFileFolder);
#if WINDOWS_PHONE_APP
#else
            DoChangeDecryptFolder(folder);
#endif
        }
        public void DoChangeDecryptFolder(IStorageFolder folder)
        {
            if (folder != null) {
                defaultOutputDecryptedFileFolder = folder;
                TextBoxDecryptedFileFolder.Text = defaultOutputDecryptedFileFolder.Name;
            } else {
                // ???
            }
        }

        private void ButtonResetDecryptFolder_Click(object sender, RoutedEventArgs e)
        {
            defaultOutputDecryptedFileFolder = null;
            TextBoxDecryptedFileFolder.Text = "";
        }


        private async void ButtonOpenOutputFile_Click(object sender, RoutedEventArgs e)
        {
            // var outputFilePath = TextBoxOutputFilePath.Text;
            // if (!String.IsNullOrEmpty(outputFilePath)) {
            //     var outputFile = await StorageFile.GetFileFromPathAsync(outputFilePath);
            if (outputFile != null) {

                // var options = new Windows.System.LauncherOptions();
                // options.DisplayApplicationPicker = true;
                // bool suc = await Launcher.LaunchFileAsync(outputFile, options);

                bool suc = await Launcher.LaunchFileAsync(outputFile);

                if (suc) {
                    // Succeeded.
                } else {
                    InstantToastHelper.Instance.ShowToast("Failed to open the output file.");
                }
            } else {
                InstantToastHelper.Instance.ShowToast("File does not exist.");
            }
            // }
        }


    }
}
