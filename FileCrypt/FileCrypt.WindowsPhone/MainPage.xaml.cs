﻿using FileCrypt.Pickers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FileCrypt
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }


        // TBD:

        public void ContinueUponFolderPicker(FolderPickerContinuationEventArgs e)
        {
            var folder = e.Folder;
            System.Diagnostics.Debug.WriteLine("ContinueUponFolderPicker() folder = {0}", folder);
            if (folder != null) {
                var data = e.ContinuationData;
                // System.Diagnostics.Debug.WriteLine("data = {0}", data);
                if (data != null) {
                    var context = (FolderPickerContext) data[PickerConstants.KEY_CONTEXT_FOR_FOLDER_PICKER];
                    if (context == FolderPickerContext.EncryptedFileFolder) {
                        UserControlFileCrypt.DoChangeEncryptFolder(folder);
                    } else if (context == FolderPickerContext.DecryptedFileFolder) {
                        UserControlFileCrypt.DoChangeDecryptFolder(folder);
                    } else {
                        // ????
                        System.Diagnostics.Debug.WriteLine("Unknown picker context = {0}", context);
                    }
                } else {
                    // ???
                    System.Diagnostics.Debug.WriteLine("ContinuationData not found.");
                }
            } else {
                // ????
            }
        }
        //public void ContinueEncryptFolderPicker(FolderPickerContinuationEventArgs e)
        //{
        //    var folder = e.Folder;
        //    System.Diagnostics.Debug.WriteLine("ContinueEncryptFolderPicker() folder = {0}", folder);
        //    if (folder != null) {
        //        UserControlFileCrypt.DoChangeEncryptFolder(folder);
        //    }
        //}
        //public void ContinueDecryptFolderPicker(FolderPickerContinuationEventArgs e)
        //{
        //    var folder = e.Folder;
        //    System.Diagnostics.Debug.WriteLine("ContinueDecryptFolderPicker() folder = {0}", folder);
        //    if (folder != null) {
        //        UserControlFileCrypt.DoChangeDecryptFolder(folder);
        //    }
        //}

        public async Task ContinueUponInputFilePickerAsync(FileOpenPickerContinuationEventArgs e)
        {
            var files = e.Files;
            if (files != null && files.Count > 0) {
                var file = files[0];
                System.Diagnostics.Debug.WriteLine("ContinueUponInputFilePickerAsync() file = {0}", file);
                if (file != null) {
                    var data = e.ContinuationData;
                    // System.Diagnostics.Debug.WriteLine("data = {0}", data);
                    if (data != null) {
                        var context = (FileOpenPickerContext) data[PickerConstants.KEY_CONTEXT_FOR_FILE_OPEN_PICKER];
                        if (context == FileOpenPickerContext.EncryptedInputFile) {
                            await UserControlFileCrypt.DoDecryptionUponFilePickAsync(file);
                        } else if (context == FileOpenPickerContext.DecryptedInputFile) {
                            await UserControlFileCrypt.DoEncryptionUponFilePickAsync(file);
                        } else {
                            // ????
                            System.Diagnostics.Debug.WriteLine("Unknown picker context = {0}", context);
                        }
                    } else {
                        // ???
                        System.Diagnostics.Debug.WriteLine("ContinuationData not found.");
                    }
                } else {
                    // ????
                }
            }
        }
        //public async Task ContinueEncryptedInputFilePickerAsync(FileOpenPickerContinuationEventArgs e)
        //{
        //    var files = e.Files;
        //    if (files != null && files.Count > 0) {
        //        var file = files[0];
        //        System.Diagnostics.Debug.WriteLine("ContinueEncryptedInputFilePickerAsync() file = {0}", file);
        //        if (file != null) {
        //            await UserControlFileCrypt.DoEncryptionUponFilePickAsync(file);
        //        }
        //    }
        //}
        //public async Task ContinueDecryptedInputFilePickerAsync(FileOpenPickerContinuationEventArgs e)
        //{
        //    var files = e.Files;
        //    if (files != null && files.Count > 0) {
        //        var file = files[0];
        //        System.Diagnostics.Debug.WriteLine("ContinueDecryptedInputFilePickerAsync() file = {0}", file);
        //        if (file != null) {
        //            await UserControlFileCrypt.DoDecryptionUponFilePickAsync(file);
        //        }
        //    }
        //}



        private void ViewboxSourceCodeIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ((App) App.Current).ShowSourceCodeInfoSettings();
        }
        private void AppBarButtonSourceCodeInfo_Click(object sender, RoutedEventArgs e)
        {
            ((App) App.Current).ShowSourceCodeInfoSettings();
        }
    }
}
