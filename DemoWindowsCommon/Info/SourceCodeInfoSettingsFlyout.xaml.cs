﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace DemoWindowsCommon.Info
{
    public sealed partial class SourceCodeInfoSettingsFlyout : SettingsFlyout
    {
        public SourceCodeInfoSettingsFlyout()
        {
            this.InitializeComponent();

            // temporary
            UserControlSourceCodeInfo.ParentSettingsFlyout = this;
            UserControlSourceCodeInfo.ShowCancelButton();
        }


        public string DefaultEmailSubject
        {
            get
            {
                return UserControlSourceCodeInfo.DefaultEmailSubject;
            }
            set
            {
                UserControlSourceCodeInfo.DefaultEmailSubject = value;
            }
        }

        public string DefaultEmailBody
        {
            get
            {
                return UserControlSourceCodeInfo.DefaultEmailBody;
            }
            set
            {
                UserControlSourceCodeInfo.DefaultEmailBody = value;
            }
        }

    
    }
}
