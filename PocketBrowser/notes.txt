

[ Windows Store App submission checklist ]
http://msdn.microsoft.com/en-us/library/windows/apps/hh694062.aspx


## Description 

A simple embedded Web browser app.  
This is one of the weekly "training apps" for practicing/promoting Windows app development.
This app illustrates the use of the Web browser control on WinRT.

Note that this app is intended to be used in the "Snap View" (docked on the left- or right-hand side of the screen with a narrow width (typically, 320~500 pixels)).



## Screenshots

1366 x 768

Mini Web browser widget.



## App Features

## Keywords

Pocket browser
Windows Runtime app development
Windows Phone 8.1 app development


## Copyright & Trademark

Copyright (c) 2015, Holo Software



## App website

http://www.windowsdemoapps.com/

## Support contact info

http://www.windowsdemoapps.com/support
holodemo@outlook.com


## Privacy policy

http://www.windowsdemoapps.com/privacy


## Legal

http://www.windowsdemoapps.com/terms
http://www.windowsdemoapps.com/legal


