﻿using SuperSticky.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SuperSticky
{
    public sealed partial class StickyNoteControl : UserControl
    {
        // temporary
        // Reference to the parent.
        private FlipView parentFlipView = null;
        // Reference to the "list" control
        private NoteListControl noteListControl = null;

        // "Data" object.
        private StickyNote? stickyNote = null;


        public StickyNoteControl()
        {
            this.InitializeComponent();

            ButtonNoteSave.IsEnabled = false;
            ButtonNoteDelete.IsEnabled = false;
            ButtonNoteNew.IsEnabled = false;
        }

        // TBD:
        public void SetParentView(ItemsControl control)
        {
            parentFlipView = (FlipView) control;
        }
        public void SetDataListControl(UserControl control)
        {
            noteListControl = (NoteListControl) control;
        }


        // TBD:
        // dirty flag???
        // ....


        // TBD:
        // Use data binding ????
        public StickyNote? StickyNote
        {
            get
            {
                return stickyNote;
            }
            set
            {
                // TBD:
                // What happens to the current stickyNote????
                // ...

                stickyNote = value;
                if (stickyNote == null) {
                    TextBoxNote.Text = "";
                    TextBoxNoteId.Text = "";
                    ButtonNoteSave.IsEnabled = false;
                    ButtonNoteNew.IsEnabled = false;
                    ButtonNoteDelete.IsEnabled = false;
                } else {
                    var id = stickyNote.Value.Id;
                    if (id > 0UL) {
                        TextBoxNote.Text = stickyNote.Value.Content;
                        TextBoxNoteId.Text = id.ToString();
                        ButtonNoteSave.IsEnabled = true;
                        ButtonNoteNew.IsEnabled = true;
                        ButtonNoteDelete.IsEnabled = true;
                    } else {
                        if (!String.IsNullOrEmpty(stickyNote.Value.Content)) {
                            TextBoxNote.Text = stickyNote.Value.Content;
                            TextBoxNoteId.Text = "";
                            ButtonNoteSave.IsEnabled = true;
                            ButtonNoteNew.IsEnabled = true;
                            ButtonNoteDelete.IsEnabled = true;
                        } else {
                            TextBoxNote.Text = "";
                            TextBoxNoteId.Text = "";
                            ButtonNoteSave.IsEnabled = false;
                            ButtonNoteNew.IsEnabled = false;
                            ButtonNoteDelete.IsEnabled = false;
                        }
                    }
                }
            }
        }



        private async void ButtonNoteSave_Click(object sender, RoutedEventArgs e)
        {
            var isUpdating = false;
            var noteId = 0UL;
            var strId = TextBoxNoteId.Text;
            if (!String.IsNullOrWhiteSpace(strId)) {
                try {
                    noteId = Convert.ToUInt64(strId);
                } catch (Exception) {
                    // Ignore.
                }
                if (noteId > 0L) {
                    isUpdating = true;
                } else {
                    // ????
                }
            } else {
                // ????
            }


            var content = TextBoxNote.Text;
            // Note the strange logic.
            // We do not allow an empty note to be created,
            // but we allow an empty content in an exting note. ??? or, just delete empty note?
            // --> Make it a user settings???
            if (!String.IsNullOrEmpty(content) || isUpdating) {
                if (isUpdating) {
                    // ???
                    if (!String.IsNullOrEmpty(content)) {
                        await NoteDataManager.Instance.UpdateNoteAsync(noteId, content);
                    } else {
                        // ????
                        await NoteDataManager.Instance.DeleteNoteAsync(noteId);
                        noteId = 0L;
                    }
                } else {
                    noteId = await NoteDataManager.Instance.CreateNoteAsync(content);
                }
            }

            if (noteId > 0L) {
                TextBoxNoteId.Text = noteId.ToString();
            } else {
                TextBoxNoteId.Text = "";
            }

            ButtonNoteSave.IsEnabled = false;
            ButtonNoteDelete.IsEnabled = true;
            ButtonNoteNew.IsEnabled = true;
        }

        private async void ButtonNoteDelete_Click(object sender, RoutedEventArgs e)
        {
            var strId = TextBoxNoteId.Text;
            if (!String.IsNullOrWhiteSpace(strId)) {
                var noteId = 0UL;
                try {
                    noteId = Convert.ToUInt64(strId);
                } catch (Exception) {
                    // Ignore.
                }
                if (noteId > 0L) {
                    await NoteDataManager.Instance.DeleteNoteAsync(noteId);
                    TextBoxNote.Text = "";
                    TextBoxNoteId.Text = "";
                } else {
                    // ????
                    TextBoxNote.Text = "";
                    TextBoxNoteId.Text = "";
                }
            } else {
                // ????
                TextBoxNote.Text = "";
                TextBoxNoteId.Text = "";
            }

            ButtonNoteSave.IsEnabled = false;
            ButtonNoteDelete.IsEnabled = false;
            ButtonNoteNew.IsEnabled = false;
        }

        private void ButtonNoteNew_Click(object sender, RoutedEventArgs e)
        {
            TextBoxNote.Text = "";
            TextBoxNoteId.Text = "";

            ButtonNoteSave.IsEnabled = false;
            ButtonNoteDelete.IsEnabled = false;
            ButtonNoteNew.IsEnabled = false;
        }

        private void TextBoxNote_TextChanged(object sender, TextChangedEventArgs e)
        {
            // ????
            var content = TextBoxNote.Text;
            if (!String.IsNullOrEmpty(content)) {
                ButtonNoteSave.IsEnabled = true;
                ButtonNoteDelete.IsEnabled = true;
                ButtonNoteNew.IsEnabled = true;
            } else {
                ButtonNoteSave.IsEnabled = false;
                ButtonNoteDelete.IsEnabled = false;
                ButtonNoteNew.IsEnabled = false;
            }
        }



    }
}
