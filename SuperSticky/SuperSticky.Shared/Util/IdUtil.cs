﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperSticky.Util
{
    public static class IdUtil
    {
        private static readonly Random RNG = new Random();

        private static readonly int MIN_L05_ID = 10000;
        private static readonly int MAX_L05_ID = 99999;
        private static readonly int MIN_L06_ID = 100000;
        private static readonly int MAX_L06_ID = 999999;
        private static readonly int MIN_L08_ID = 10000000;
        private static readonly int MAX_L08_ID = 99999999;

        // Cannot ensure global uniqueness, but it does the job.
        private static HashSet<ulong> idCache = new HashSet<ulong>();


        public static string GetNewGuid()
        {
            var guid = Guid.NewGuid();
            var strGuid = guid.ToString();
            return strGuid;
        }


        public static ulong GetNextUniqueId()
        {
            return GetNextUniqueId06();
        }
        public static ulong GetNextUniqueId05()
        {
            var id = 0UL;
            do {
                id = (ulong) RNG.Next(MIN_L05_ID, MAX_L05_ID);
            } while (idCache.Contains(id));
            return id;
        }
        public static ulong GetNextUniqueId06()
        {
            var id = 0UL;
            do {
                id = (ulong) RNG.Next(MIN_L06_ID, MAX_L06_ID);
            } while (idCache.Contains(id));
            return id;
        }
        public static ulong GetNextUniqueId08()
        {
            var id = 0UL;
            do {
                id = (ulong) RNG.Next(MIN_L08_ID, MAX_L08_ID);
            } while (idCache.Contains(id));
            return id;
        }


    }
}
