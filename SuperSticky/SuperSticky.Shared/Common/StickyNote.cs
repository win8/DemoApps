﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperSticky.Common
{
    public struct StickyNote
    {
        private ulong id;        // Valid id > 0L
        private string title;
        private string content;
        private long created;
        private long updated;

        public StickyNote(ulong id)
        {
            this.id = id;
            this.title = null;
            this.content = null;
            this.created = 0L;
            this.updated = 0L;
        }

        public ulong Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
            }
        }
        public long Created
        {
            get
            {
                return created;
            }
            set
            {
                created = value;
            }
        }
        public long Updated
        {
            get
            {
                return updated;
            }
            set
            {
                updated = value;
            }
        }


    }
}
