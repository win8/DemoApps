﻿using SuperSticky.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace SuperSticky.Common
{
    public class NoteDataManager
    {
        // temporary
        private static readonly string NOTE_FILENAME_PREFIX = "Note-";
        private static readonly string NOTE_FILENAME_INFIX = "-";

        private static NoteDataManager instance = null;
        public static NoteDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new NoteDataManager();
                }
                return instance;
            }
        }
        private NoteDataManager()
        {
        }


        // TBD: Store note metadata in Settings???
        // ....
        // Map <id -> guid>
        // private Dictionary<long, string> IdGuidMap = new Dictionary<long, string>();
        // ...


        // TBD:
        // Instead of using the metadata Settings,
        // we just "store" the note metadata in the file name.
        // -->
        // Actually this scheme does not work
        // sicne the file name cannot be uniquely determined given the id (id only).

        private static string GetNoteFileName(ulong id)
        {
            return NOTE_FILENAME_PREFIX + id;
        }
        private static string GetNoteFileName(ulong id, long created)
        {
            return NOTE_FILENAME_PREFIX + id + NOTE_FILENAME_INFIX + created;
            // return GetNoteFileName(id) + NOTE_FILENAME_INFIX + created;
        }
        private static string GetNoteFileName(ulong id, long created, long updated)
        {
            return NOTE_FILENAME_PREFIX + id + NOTE_FILENAME_INFIX + created + NOTE_FILENAME_INFIX + updated;
            // return GetNoteFileName(id, created) + NOTE_FILENAME_INFIX + updated;
        }

        private static ulong GetIdFromFileName(string name)
        {
            var id = 0UL;
            if (name != null && name.Length > NOTE_FILENAME_PREFIX.Length) {
                try {
                    var strId = name.Substring(NOTE_FILENAME_PREFIX.Length);
                    var idx = strId.IndexOf("-");
                    if (idx > 0) {
                        strId = strId.Substring(0, idx);
                    }
                    id = Convert.ToUInt64(strId);
                } catch (Exception) {
                    // Ignore...
                }
            }
            return id;
        }
        private static long GetCreatedTimeFromFileName(string name)
        {
            var created = 0L;
            if (name != null && name.Length > NOTE_FILENAME_PREFIX.Length) {
                try {
                    var strId = name.Substring(NOTE_FILENAME_PREFIX.Length);
                    var idx0 = strId.IndexOf("-");
                    if (idx0 > 0) {
                        var strCreated = strId.Substring(idx0 + 1);
                        var idx1 = strCreated.IndexOf("-");
                        if (idx1 > 0) {
                            strCreated = strCreated.Substring(0, idx1);
                        }
                        created = Convert.ToInt64(strCreated);
                    }
                } catch (Exception) {
                    // Ignore...
                }
            }
            return created;
        }
        private static long GetUpdatedTimeFromFileName(string name)
        {
            var updated = 0L;
            if (name != null && name.Length > NOTE_FILENAME_PREFIX.Length) {
                try {
                    var strId = name.Substring(NOTE_FILENAME_PREFIX.Length);
                    var idx0 = strId.IndexOf("-");
                    if (idx0 > 0) {
                        var strCreated = strId.Substring(idx0 + 1);
                        var idx1 = strCreated.IndexOf("-");
                        if (idx1 > 0) {
                            var strUpdated = strCreated.Substring(idx1 + 1);
                            updated = Convert.ToInt64(strUpdated);
                        }
                    }
                } catch (Exception) {
                    // Ignore...
                }
            }
            return updated;
        }


        // temporary
        private static readonly int MAX_TITLE_LENGTH = 26;
        public static string GetTitle(string content)
        {
            return GetTitle(content, MAX_TITLE_LENGTH);
        }
        public static string GetTitle(string content, int maxLength)
        {
            string title = null;
            if (content != null && content.Length > 0) {
                var firstline = content;
                if (content.Contains(Environment.NewLine)) {
                    firstline = content.Substring(0, content.IndexOf(Environment.NewLine));
                }
                // int size = Math.Min(maxLength, firstline.Length);
                // title = firstline.Substring(0, size);
                if (firstline.Length > maxLength) {
                    title = firstline.Substring(0, maxLength - 3) + "...";
                } else {
                    title = firstline;
                }
            }
            return title;
        }


        public async Task<StickyNote> GetStickyNoteAsync(ulong id)
        {
            StickyNote note = new StickyNote(id);
            var content = await GetNoteAsync(id);
            if (content != null) {
                note.Content = content;
                var title = GetTitle(content);
                if (title != null) {
                    note.Title = title;
                }
            }
            return note;
        }


        public async Task<List<StickyNote>> GetStickyNotesAsync(int count)
        {
            var noteList = new List<StickyNote>();
            var allNotes = await GetAllNotesAsync();
            if (allNotes != null && allNotes.Count > 0) {
                var size = Math.Min(count, allNotes.Count);
                for (var i = 0; i < size; i++) {
                    var id = allNotes[i];
                    StickyNote note = new StickyNote(id);
                    var content = await GetNoteAsync(id);
                    if (content != null) {
                        note.Content = content;
                        var title = GetTitle(content);
                        if (title != null) {
                            note.Title = title;
                        }
                    }
                    noteList.Add(note);
                }
                // Sort???
                // noteList = noteList.OrderByDescending(n => n.updated);
                // ....
                // --> We have to really sort the original list (that includes all items).
                // But, the original list is a list of ulongs, not sticky notes.
                // ....
            }
            return noteList;
        }

        public async Task<List<ulong>> GetAllNotesAsync()
        {
            var notes = new List<ulong>();
            var files = await AppDataHelper.Instance.GetAppDataFolder().GetFilesAsync();
            if (files != null && files.Count > 0) {
                foreach (var f in files) {
                    var id = GetIdFromFileName(f.Name);
                    if (id > 0UL) {
                        notes.Add(id);
                    } else {
                        // ???
                    }
                }
                // tbd:
                // sort ????
                // ...
                // notes = notes.OrderByDescending(n => n.updated);
                // ????
            }
            return notes;
        }


        public async Task<string> GetNoteAsync(ulong id)
        {
            string content = null;
            var noteFileName = GetNoteFileName(id);
            var file = await AppDataHelper.Instance.GetAppDataFolder().GetFileAsync(noteFileName);
            if (file != null) {
                content = await FileIO.ReadTextAsync(file);
            } else {
                // ????
            }
            return content;
        }

        public async Task<ulong> CreateNoteAsync(string content)
        {
            var id = IdUtil.GetNextUniqueId();
            // TBD: Add created time?
            // var now = DateTimeUtil.CurrentUnixEpochMillis();
            // ....
            var noteFileName = GetNoteFileName(id);
            var file = await AppDataHelper.Instance.GetAppDataFolder().CreateFileAsync(noteFileName, CreationCollisionOption.ReplaceExisting);
            if (file != null) {
                await FileIO.WriteTextAsync(file, content);
                return id;
            } else {
                System.Diagnostics.Debug.WriteLine("Failed to create a file.");
                return 0UL;
            }
        }

        public async Task UpdateNoteAsync(ulong id, string content)
        {
            var noteFileName = GetNoteFileName(id);
            // TBD: Add updated time?
            // var now = DateTimeUtil.CurrentUnixEpochMillis();
            // ....
            var file = await AppDataHelper.Instance.GetAppDataFolder().GetFileAsync(noteFileName);
            if (file != null) {
                await FileIO.WriteTextAsync(file, content);
            } else {
                System.Diagnostics.Debug.WriteLine("Failed to update the file: id = {0}", id);
            }
        }

        public async Task DeleteNoteAsync(ulong id)
        {
            var noteFileName = GetNoteFileName(id);
            var file = await AppDataHelper.Instance.GetAppDataFolder().GetFileAsync(noteFileName);
            if (file != null) {
                await file.DeleteAsync();
            } else {
                System.Diagnostics.Debug.WriteLine("Failed to delete file: id = {0}", id);
            }
        }

    }

}
