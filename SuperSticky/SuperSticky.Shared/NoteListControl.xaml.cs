﻿using SuperSticky.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SuperSticky
{
    public sealed partial class NoteListControl : UserControl
    {
        // temporary
        // Reference to the parent.
        private FlipView parentFlipView = null;
        // Reference to the "edit" control
        private StickyNoteControl stickyNoteControl = null;

        // Primarily for "Edit" buttons.
        private Dictionary<int, ulong> idDict = new Dictionary<int, ulong>();

        public NoteListControl()
        {
            this.InitializeComponent();

            // TBD:
            // How to refresh this after the page has been loaded ????
            RefreshDataAndControl();
        }

        // TBD:
        public void SetParentView(ItemsControl control)
        {
            parentFlipView = (FlipView) control;
        }
        public void SetDataEditControl(UserControl control)
        {
            stickyNoteControl = (StickyNoteControl) control;
        }

        // temporary
        public async void RefreshDataAndControl()
        {
            System.Diagnostics.Debug.WriteLine("RefreshDataAndControl() called.");

            var notes = await NoteDataManager.Instance.GetStickyNotesAsync(4);
            var size = notes.Count;

            // temporary
            // (for testing)
            for (var i = 0; i < size; i++) {
                var id = notes[i].Id;
                var title = notes[i].Title;
                var content = notes[i].Content;
                // await NoteDataManager.Instance.DeleteNoteAsync(id);
                System.Diagnostics.Debug.WriteLine("Note id: {0}, Title: {1}, Content: {2}", id, title, content);
            }
            // size = 0;
            // temporary


            if (size > 0) {
                var sn0 = notes[0];
                var id0 = sn0.Id;

                idDict[0] = id0;
                var content0 = sn0.Content;
                var title0 = sn0.Title;
                if (String.IsNullOrWhiteSpace(title0)) {
                    title0 = String.Format("({0})", id0);
                }
                TextBoxEditNote1.Text = title0;
                ButtonEditNote1.IsEnabled = true;
                if (size > 1) {
                    var sn1 = notes[1];
                    var id1 = sn1.Id;

                    idDict[1] = id1;
                    var content1 = sn1.Content;
                    var title1 = sn1.Title;
                    if (String.IsNullOrWhiteSpace(title1)) {
                        title1 = String.Format("({0})", id1);
                    }
                    TextBoxEditNote2.Text = title1;
                    ButtonEditNote2.IsEnabled = true;
                    if (size > 2) {
                        var sn2 = notes[2];
                        var id2 = sn2.Id;

                        idDict[2] = id2;
                        var content2 = sn2.Content;
                        var title2 = sn2.Title;
                        if (String.IsNullOrWhiteSpace(title2)) {
                            title2 = String.Format("({0})", id2);
                        }
                        TextBoxEditNote3.Text = title2;
                        ButtonEditNote3.IsEnabled = true;
                        if (size > 3) {
                            var sn3 = notes[3];
                            var id3 = sn3.Id;

                            idDict[3] = id3;
                            var content3 = sn3.Content;
                            var title3 = sn3.Title;
                            if (String.IsNullOrWhiteSpace(title3)) {
                                title3 = String.Format("({0})", id3);
                            }
                            TextBoxEditNote4.Text = title3;
                            ButtonEditNote4.IsEnabled = true;
                        } else {
                            idDict.Remove(3);
                            TextBoxEditNote4.Text = "";
                            ButtonEditNote4.IsEnabled = false;
                        }
                    } else {
                        idDict.Remove(2);
                        idDict.Remove(3);
                        TextBoxEditNote3.Text = "";
                        ButtonEditNote3.IsEnabled = false;
                        TextBoxEditNote4.Text = "";
                        ButtonEditNote4.IsEnabled = false;
                    }
                } else {
                    idDict.Remove(1);
                    idDict.Remove(2);
                    idDict.Remove(3);
                    TextBoxEditNote2.Text = "";
                    ButtonEditNote2.IsEnabled = false;
                    TextBoxEditNote3.Text = "";
                    ButtonEditNote3.IsEnabled = false;
                    TextBoxEditNote4.Text = "";
                    ButtonEditNote4.IsEnabled = false;
                }
            } else {
                idDict.Clear();
                TextBoxEditNote1.Text = "";
                ButtonEditNote1.IsEnabled = false;
                TextBoxEditNote2.Text = "";
                ButtonEditNote2.IsEnabled = false;
                TextBoxEditNote3.Text = "";
                ButtonEditNote3.IsEnabled = false;
                TextBoxEditNote4.Text = "";
                ButtonEditNote4.IsEnabled = false;
            }
        }

        private async void ButtonEditNote1_Click(object sender, RoutedEventArgs e)
        {
            if (idDict.ContainsKey(0)) {
                var id0 = idDict[0];
                StickyNote? note0 = await NoteDataManager.Instance.GetStickyNoteAsync(id0);
                if (note0 != null) {
                    var stickyNote0 = note0.Value;
                    if (stickyNoteControl != null) {
                        stickyNoteControl.StickyNote = stickyNote0;
                        if (parentFlipView != null) {
                            parentFlipView.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private async void ButtonEditNote2_Click(object sender, RoutedEventArgs e)
        {
            if (idDict.ContainsKey(1)) {
                var id1 = idDict[1];
                StickyNote? note1 = await NoteDataManager.Instance.GetStickyNoteAsync(id1);
                if (note1 != null) {
                    var stickyNote1 = note1.Value;
                    if (stickyNoteControl != null) {
                        stickyNoteControl.StickyNote = stickyNote1;
                        if (parentFlipView != null) {
                            parentFlipView.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private async void ButtonEditNote3_Click(object sender, RoutedEventArgs e)
        {
            if (idDict.ContainsKey(2)) {
                var id2 = idDict[2];
                StickyNote? note2 = await NoteDataManager.Instance.GetStickyNoteAsync(id2);
                if (note2 != null) {
                    var stickyNote2 = note2.Value;
                    if (stickyNoteControl != null) {
                        stickyNoteControl.StickyNote = stickyNote2;
                        if (parentFlipView != null) {
                            parentFlipView.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private async void ButtonEditNote4_Click(object sender, RoutedEventArgs e)
        {
            if (idDict.ContainsKey(3)) {
                var id3 = idDict[3];
                StickyNote? note3 = await NoteDataManager.Instance.GetStickyNoteAsync(id3);
                if (note3 != null) {
                    var stickyNote3 = note3.Value;
                    if (stickyNoteControl != null) {
                        stickyNoteControl.StickyNote = stickyNote3;
                        if (parentFlipView != null) {
                            parentFlipView.SelectedIndex = 0;
                        }
                    }
                }
            }
        }
    }
}
