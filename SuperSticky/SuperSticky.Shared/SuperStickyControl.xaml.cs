﻿using SuperSticky.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SuperSticky
{
    public sealed partial class SuperStickyControl : UserControl
    {
        public SuperStickyControl()
        {
            this.InitializeComponent();

            UserControlStickyNote.SetParentView(FlipViewLayoutRoot);
            UserControlStickyNote.SetDataListControl(UserControlNoteList);
            UserControlNoteList.SetParentView(FlipViewLayoutRoot);
            UserControlNoteList.SetDataEditControl(UserControlStickyNote);
        }

        private void FlipViewLayoutRoot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FlipViewLayoutRoot != null) {
                // var idx = FlipViewLayoutRoot.SelectedIndex;

                NoteListControl noteListControl = FlipViewLayoutRoot.SelectedItem as NoteListControl;
                if (noteListControl != null) {
                    noteListControl.RefreshDataAndControl();
                }
            }

        }

        public void SelectItemInFlipView(UserControl item)
        {
            FlipViewLayoutRoot.SelectedItem = item;

            // ...

        }

        public void SetStickyNoteOnEditor(StickyNote stickyNote)
        {
            // TBD:
            // This should be done only if the stickyNote on the editor is "clean".
            // ????

            // This is a bit fragile. If the UI/layout changes, then this needs to change as well.
            StickyNoteControl stickyNoteControl = FlipViewLayoutRoot.Items[0] as StickyNoteControl;
            if (stickyNoteControl != null) {
                stickyNoteControl.StickyNote = stickyNote;
            }
        }



    }
}
