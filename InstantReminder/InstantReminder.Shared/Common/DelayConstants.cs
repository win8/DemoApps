﻿using System;
using System.Collections.Generic;
using System.Text;


namespace InstantReminder.Common
{
    public enum DelayLength
    {
        NoDelay,  // Not being used.
        Mins_10,
        Mins_20,
        Mins_30,
        Hours_1,
        Hours_2,
        Hours_3,
        Hours_6
        // Hours_12,
        // Days_1
    }
    public static class DelayConstants
    {
        public static DelayLength[] DelayLengths = new DelayLength[] { 
            DelayLength.Mins_10,
            DelayLength.Mins_20,
            DelayLength.Mins_30,
            DelayLength.Hours_1,
            DelayLength.Hours_2,
            DelayLength.Hours_3,
            DelayLength.Hours_6
            // DelayLength.Hours_12,
            // DelayLength.Days_1
        };

        public static string LengthToString(DelayLength length)
        {
            string str = null;
            switch (length) {
                case DelayLength.Mins_10:
                    str = "10 Minutes";
                    break;
                case DelayLength.Mins_20:
                    str = "20 Minutes";
                    break;
                case DelayLength.Mins_30:
                    str = "30 Minutes";
                    break;
                case DelayLength.Hours_1:
                    str = "1 Hour";
                    break;
                case DelayLength.Hours_2:
                    str = "2 Hours";
                    break;
                case DelayLength.Hours_3:
                    str = "3 Hours";
                    break;
                case DelayLength.Hours_6:
                    str = "6 Hours";
                    break;
                //case DelayLength.Hours_12:
                //    str = "12 Hours";
                //    break;
                //case DelayLength.Days_1:
                //    str = "1 Day";
                //    break;
                case DelayLength.NoDelay:
                default:
                    str = "";
                    break;
            }

            return str;
        }

        public static string[] DelayLengthArray()
        {
            var arr = new string[DelayLengths.Length];
            for (int i = 0; i < DelayLengths.Length; i++) {
                DelayLength dl = DelayLengths[i];
                arr[i] = LengthToString(dl);
            }
            return arr;
        }

        public static int GetDelayInMinutes(DelayLength length)
        {
            int mins = 0;
            switch (length) {
                case DelayLength.Mins_10:
                    mins = 10;
                    break;
                case DelayLength.Mins_20:
                    mins = 20;
                    break;
                case DelayLength.Mins_30:
                    mins = 30;
                    break;
                case DelayLength.Hours_1:
                    mins = 60;
                    break;
                case DelayLength.Hours_2:
                    mins = 2 * 60;
                    break;
                case DelayLength.Hours_3:
                    mins = 3 * 60;
                    break;
                case DelayLength.Hours_6:
                    mins = 6 * 60;
                    break;
                //case DelayLength.Hours_12:
                //    mins = 12 * 60;
                //    break;
                //case DelayLength.Days_1:
                //    mins = 24 * 60;
                //    break;
                case DelayLength.NoDelay:
                default:
                    break;
            }

            return mins;
        }
    }

}

