﻿using InstantReminder.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace InstantReminder
{
    public sealed partial class InstantReminderControl : UserControl
    {
        // temporary
        private static DelayLength DefaultDelayLength = DelayLength.Mins_30;

        public InstantReminderControl()
        {
            this.InitializeComponent();

            // ???
            ComboBoxDelay.DataContext = DelayConstants.DelayLengthArray();
            // ???
            ComboBoxDelay.SelectedItem = DelayConstants.LengthToString(DefaultDelayLength);
        }


        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            var delayLength = DefaultDelayLength;
            int sIdx = ComboBoxDelay.SelectedIndex;
            if (sIdx >= 0 && sIdx < DelayConstants.DelayLengths.Length) {
                delayLength = DelayConstants.DelayLengths[sIdx];
            }
            var note = TextBoxNote.Text;

            int delayInMinutes = DelayConstants.GetDelayInMinutes(delayLength);

            // if (delayInMinutes > 0 && !String.IsNullOrWhiteSpace(note)) {
            if (delayInMinutes > 0) {
                if (String.IsNullOrWhiteSpace(note)) {
                    note = "";
                } else {
                    note = note.Trim();
                }

                try {
                    ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
                    XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
                    XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");
                    toastTextElements[0].AppendChild(toastXml.CreateTextNode("Demo App Reminder:"));
                    toastTextElements[1].AppendChild(toastXml.CreateTextNode(note));

                    var deliveryTime = DateTimeOffset.Now.AddMinutes(delayInMinutes);
                    // for testing...
                    // var deliveryTime = DateTimeOffset.Now.AddSeconds(delayInMinutes);
                    // ...
                    var toast = new ScheduledToastNotification(toastXml, deliveryTime);
                    ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);

                    // ???
                    ShowConfirmationToast();
                    ClearReminderForm();
                } catch (Exception ex) {
                    // Ignore.
                    var errorMsg = "Error occurred: " + ex.Message;
                    ShowInstantToast(errorMsg);
                }
            } else {
                // ????
            }
        }

        private void ShowConfirmationToast()
        {
            var message = "Reminder has been saved.";
            ShowInstantToast(message);
        }
        private void ShowInstantToast(string message)
        {
            try {
                ToastTemplateType toastTemplate = ToastTemplateType.ToastText01;
                XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
                XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");
                toastTextElements[0].AppendChild(toastXml.CreateTextNode(message));

                var toast = new ToastNotification(toastXml);
                ToastNotificationManager.CreateToastNotifier().Show(toast);
            } catch (Exception) {
                // Ignore
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            ClearReminderForm();
        }

        private void ClearReminderForm()
        {
            ComboBoxDelay.SelectedItem = DelayConstants.LengthToString(DefaultDelayLength);
            TextBoxNote.Text = "";
        }

    }


}
