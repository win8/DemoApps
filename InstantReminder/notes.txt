

[ Windows Store App submission checklist ]
http://msdn.microsoft.com/en-us/library/windows/apps/hh694062.aspx


## Description 

A simple reminder app. The user can set a delayed reminder, which is displayed as a system toast message.
This is the fourth one from a series of demo apps we plan to develop for Windows app development training purposes.

Note that this app is intended to be used in the "Snap View" (docked on the left- or right-hand side of the screen with a narrow width (typically, 320~500 pixels)).


## App Features

## Keywords

Simple reminder
Windows runtime app development
Windows Phone 8.1 app development


## Copyright & Trademark

Copyright (c) 2015, Pico Software


## Screenshots

1366 x 766

Mini reminder widget.


## App website

http://www.windowsdemoapps.com/

## Support contact info

http://www.windowsdemoapps.com/support
picosoftware@outlook.com


## Privacy policy

http://www.windowsdemoapps.com/privacy


## Legal

http://www.windowsdemoapps.com/legal



