﻿using System;
using System.Collections.Generic;
using System.Text;


namespace PortableCalculator.Common
{
    public enum UnaryOperator
    {
        None = 0,
        Negate,     // -X
        Inverse,    // 1/X
        Percent,
        SquareRoot,
        Square,
        Cubic
    }
    public enum BinaryOperator
    {
        None = 0,
        Add,
        Subtract,
        Multiply,
        Divide
    }

    public interface ICalculator
    {
        decimal CurrentValue
        {
            get;
            set;
        }

        decimal Add(decimal operand);
        decimal Subtract(decimal operand);
        decimal Multiply(decimal operand);
        decimal Divide(decimal operand);

        decimal Negate();
        // decimal Negate(decimal number);
        decimal Inverse();
        // decimal Inverse(decimal number);
        decimal Percent();
        // decimal Percent(decimal number);
        decimal SquareRoot();
        // decimal SquareRoot(decimal number);
        decimal Square();
        // decimal Square(decimal number);
        decimal Cubic();
        // decimal Cubic(decimal number);

        void Clear();

    }

    // "Basic" calculator
    // Supports *,/,+,-, and 1/x, %, and square root.
    public class BasicCalculator : ICalculator, IUndoable
    {
        private decimal currentValue;
        private Stack<decimal> previousValues;

        public BasicCalculator()
        {
            currentValue = 0.0m;
            previousValues = new Stack<decimal>();
        }

        public decimal CurrentValue
        {
            get
            {
                return currentValue;
            }
            set
            {
                // previousValues.Push(currentValue);
                currentValue = value;
            }
        }

        public decimal doBinaryOperation(BinaryOperator op, decimal operand)
        {
            decimal result = 0m;
            switch (op) {
                case BinaryOperator.Add:
                    result = Add(operand);
                    break;
                case BinaryOperator.Subtract:
                    result = Subtract(operand);
                    break;
                case BinaryOperator.Multiply:
                    result = Multiply(operand);
                    break;
                case BinaryOperator.Divide:
                    result = Divide(operand);
                    break;
                default:
                    // ????
                    break;
            }
            return result;
        }

        public decimal Add(decimal operand)
        {
            previousValues.Push(currentValue);
            currentValue += operand;
            return currentValue;
        }
        public decimal Subtract(decimal operand)
        {
            previousValues.Push(currentValue);
            currentValue -= operand;
            return currentValue;
        }
        public decimal Multiply(decimal operand)
        {
            previousValues.Push(currentValue);
            currentValue *= operand;
            return currentValue;
        }
        public decimal Divide(decimal operand)
        {
            if (operand == 0.0m) {
                // What to do ????
                throw new ArgumentException("Divide-by-zero error.");
            }
            previousValues.Push(currentValue);
            currentValue /= operand;
            return currentValue;
        }

        public decimal Negate()
        {
            previousValues.Push(currentValue);
            currentValue *= -1m;
            return currentValue;
        }
        public decimal Negate(decimal number)
        {
            return (-1m * number);
        }
        public decimal Inverse()
        {
            if (currentValue == 0.0m) {
                // What to do ????
                throw new ArgumentException("Zero cannot be inversed.");
            }
            previousValues.Push(currentValue);
            currentValue = 1.0m / currentValue;
            return currentValue;
        }
        public decimal Inverse(decimal number)
        {
            if (number == 0.0m) {
                // What to do ????
                throw new ArgumentException("Zero cannot be inversed.");
            }
            return (1.0m / number);
        }

        public decimal Percent()
        {
            previousValues.Push(currentValue);
            currentValue /= 100.0m;
            return currentValue;
        }
        public decimal Percent(decimal number)
        {
            return number / 100.0m;
        }

        public decimal SquareRoot()
        {
            if (currentValue < 0m) {
                throw new ArgumentException("Cannot take a square root of a negative value.");
            }
            previousValues.Push(currentValue);
            // ????
            currentValue = (decimal) Math.Sqrt((double) currentValue);
            // ????
            return currentValue;
        }
        public decimal SquareRoot(decimal number)
        {
            if (number < 0m) {
                throw new ArgumentException("Cannot take a square root of a negative value.");
            }
            // ????
            return (decimal) Math.Sqrt((double) number);
        }

        public decimal Square()
        {
            previousValues.Push(currentValue);
            currentValue = currentValue * currentValue;
            return currentValue;
        }
        public decimal Square(decimal number)
        {
            return number * number;
        }

        public decimal Cubic()
        {
            previousValues.Push(currentValue);
            currentValue = currentValue * currentValue * currentValue;
            return currentValue;
        }
        public decimal Cubic(decimal number)
        {
            return number * number * number;
        }


        public void Clear()
        {
            currentValue = 0.0m;
            previousValues.Clear();    // ?????
        }


        public void Undo()
        {
            if (previousValues.Count > 0) {
                currentValue = previousValues.Pop();
            } else {
                // ???
                currentValue = 0.0m;
            }
        }

    }
}
