﻿using System;
using System.Collections.Generic;
using System.Text;


namespace PortableCalculator.Common
{
    public interface IUndoable
    {
        void Undo();
    }
}
