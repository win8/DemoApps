﻿using PortableCalculator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236


namespace PortableCalculator
{
    // Note:
    // The current implementation is slightly different from Windows standard calculator.
    // For instance, 
    // (1) 10 * = = yields 1,000 on Windows calc.
    // whereas it generates 10,000 in the current implementation.
    // (2) CE bebavior is slightly different. See the comment below.
    // ...

    public sealed partial class PortableCalculatorControl : UserControl
    {
        // Note that, potentially, we can have multiple instances of ICalculator in a calculatorControl..
        private BasicCalculator basicCalulator;
        private string currentEntryStr;
        private BinaryOperator currentOperator;

        //// TBD: This "basic" calculator seems pretty strange.
        //// We are implementing the calculator
        ////    based on the behavior of the calculator app on Windows.
        private bool isShowingResult;
        // isShowingResult == true -> The display shows the current value.
        // isShowingResult == false -> The display shows the current operand (e.g. the user is still typing, etc.)
        // isShowingResult == true <--> currentOperator == BinaryOperator.None   ???
        // isShowingResult == false <--> currentOperator != BinaryOperator.None  ???
        // --> Actually this is not true...

        public PortableCalculatorControl()
        {
            this.InitializeComponent();

            basicCalulator = new BasicCalculator();
            currentOperator = BinaryOperator.None;
            currentEntryStr = "";
            TextBoxNumberDisplay.Text = "0";

            isShowingResult = false;

            // Square Root character.
            var squareRootChar = Convert.ToChar(0x221A);
            ButtonMiscSquareRoot.Content = squareRootChar.ToString();
        }


        private decimal GetCurrentDisplayNumber()
        {
            var str = TextBoxNumberDisplay.Text;
            return GetCurrentDisplayNumber(str);
        }
        private decimal GetCurrentDisplayNumber(String str)
        {
            var val = 0m;
            try {
                val = Convert.ToDecimal(str);
            } catch (Exception) {
                // Ignore...
            }
            return val;
        }
        private bool IsCurrentDisplayEmptyOrZero()
        {
            var str = TextBoxNumberDisplay.Text;
            if (String.IsNullOrEmpty(str)) {
                return true;
            } else {
                if (str != null && str.Contains(".")) {   // "0.000" is NOT zero according to this function.
                    return false;
                } else {
                    var val = GetCurrentDisplayNumber(str);
                    return (val == 0m);
                }
            }
        }

        private decimal GetCurrentEntryValue()
        {
            if (String.IsNullOrWhiteSpace(currentEntryStr)) {
                currentEntryStr = "0";
            }
            var num = 0m;
            try {
                num = Convert.ToDecimal(currentEntryStr);
            } catch (Exception) {
                // Ignore
            }
            return num;
        }
        private bool IsCurrentEntryEmpty()
        {
            return String.IsNullOrEmpty(currentEntryStr);
        }
        private bool IsCurrentEntryEmptyOrZero()
        {
            if (String.IsNullOrEmpty(currentEntryStr)) {
                return true;
            } else {
                // if (currentEntryStr != null && currentEntryStr.Contains(".")) {   // "0.000" is NOT zero according to this function.
                //     return false;
                // } else {
                var val = GetCurrentEntryValue();
                return (val == 0m);
                // }
            }
        }


        private void ButtonDigit0_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "0";
                } else {
                    currentEntryStr += "0";
                }
            } else {
                currentEntryStr = "0";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit1_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "1";
                } else {
                    currentEntryStr += "1";
                }
            } else {
                currentEntryStr = "1";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit2_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "2";
                } else {
                    currentEntryStr += "2";
                }
            } else {
                currentEntryStr = "2";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit3_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "3";
                } else {
                    currentEntryStr += "3";
                }
            } else {
                currentEntryStr = "3";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit4_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "4";
                } else {
                    currentEntryStr += "4";
                }
            } else {
                currentEntryStr = "4";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit5_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "5";
                } else {
                    currentEntryStr += "5";
                }
            } else {
                currentEntryStr = "5";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit6_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "6";
                } else {
                    currentEntryStr += "6";
                }
            } else {
                currentEntryStr = "6";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit7_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "7";
                } else {
                    currentEntryStr += "7";
                }
            } else {
                currentEntryStr = "7";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit8_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "8";
                } else {
                    currentEntryStr += "8";
                }
            } else {
                currentEntryStr = "8";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }
        private void ButtonDigit9_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    //if (currentEntryStr != null && currentEntryStr.Contains(".")) {
                    //    currentEntryStr += "9";
                    //} else {
                    currentEntryStr = "9";
                    //}
                } else {
                    currentEntryStr += "9";
                }
            } else {
                currentEntryStr = "9";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }


        private void ButtonSymbolDot_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult == false) {
                if (IsCurrentDisplayEmptyOrZero() == true) {
                    currentEntryStr = "0.";
                } else {
                    if (currentEntryStr.Contains(".")) {
                        // Ignore
                    } else {
                        currentEntryStr += ".";
                    }
                }
            } else {
                currentEntryStr = "0.";
                isShowingResult = false;
            }
            RefreshNumberDisplay();
        }


        private void ButtonOpInverse_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (currentOperator == BinaryOperator.None) {
                    var currentVal = GetCurrentDisplayNumber();
                    var inverted = basicCalulator.Inverse(currentVal);
                    basicCalulator.CurrentValue = inverted;
                    currentEntryStr = "";
                    isShowingResult = true;
                } else {
                    if (isShowingResult == true) {
                        var currentValue = basicCalulator.Inverse();
                        currentOperator = BinaryOperator.None;
                        currentEntryStr = "";
                    } else {
                        var currentVal = GetCurrentDisplayNumber();
                        var inverted = basicCalulator.Inverse(currentVal);
                        currentEntryStr = inverted.ToString();
                    }
                }
                RefreshNumberDisplay();
                RefreshOperatorButtons();
            } catch (Exception ex) {
                ToastNotificationHelper.Instance.ShowToast("Invalid operation: " + ex.Message);
            }
        }

        private void ButtonOpPercent_Click(object sender, RoutedEventArgs e)
        {
            if (currentOperator == BinaryOperator.None) {
                var currentVal = GetCurrentDisplayNumber();
                var computed = basicCalulator.Percent(currentVal);
                basicCalulator.CurrentValue = computed;
                currentEntryStr = "";
                isShowingResult = true;
            } else {
                if (isShowingResult == true) {
                    var currentValue = basicCalulator.Percent();
                    currentOperator = BinaryOperator.None;
                    currentEntryStr = "";
                } else {
                    var currentVal = GetCurrentDisplayNumber();
                    var computed = basicCalulator.Percent(currentVal);
                    currentEntryStr = computed.ToString();
                }
            }
            RefreshNumberDisplay();
            RefreshOperatorButtons();
        }

        private void ButtonMiscSquareRoot_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (currentOperator == BinaryOperator.None) {
                    var currentVal = GetCurrentDisplayNumber();
                    var computed = basicCalulator.SquareRoot(currentVal);
                    basicCalulator.CurrentValue = computed;
                    currentEntryStr = "";
                    isShowingResult = true;
                } else {
                    if (isShowingResult == true) {
                        var currentValue = basicCalulator.SquareRoot();
                        currentOperator = BinaryOperator.None;
                        currentEntryStr = "";
                    } else {
                        var currentVal = GetCurrentDisplayNumber();
                        var computed = basicCalulator.SquareRoot(currentVal);
                        currentEntryStr = computed.ToString();
                    }
                }
                RefreshNumberDisplay();
                RefreshOperatorButtons();
            } catch (Exception ex) {
                ToastNotificationHelper.Instance.ShowToast("Invalid operation: " + ex.Message);
            }
        }


        private void doComputation()
        {
            if (currentOperator == BinaryOperator.None) {
                // Make the current entry the current value.
                basicCalulator.CurrentValue = GetCurrentDisplayNumber();
                currentEntryStr = "";
                isShowingResult = true;
            } else {
                if (IsCurrentEntryEmpty()) {
                    // Note: We ignore the current operator.
                    // Do nothing.
                } else {
                    var currentEntry = GetCurrentEntryValue();
                    var currentValue = basicCalulator.doBinaryOperation(currentOperator, currentEntry);
                    currentEntryStr = "";
                    isShowingResult = true;
                }
            }
            RefreshNumberDisplay();
        }

        private void ButtonOpPlus_Click(object sender, RoutedEventArgs e)
        {
            doComputation();
            currentOperator = BinaryOperator.Add;
            RefreshOperatorButtons();
        }

        private void ButtonOpMinus_Click(object sender, RoutedEventArgs e)
        {
            doComputation();
            currentOperator = BinaryOperator.Subtract;
            RefreshOperatorButtons();
        }

        private void ButtonOpMultiply_Click(object sender, RoutedEventArgs e)
        {
            doComputation();
            currentOperator = BinaryOperator.Multiply;
            RefreshOperatorButtons();
        }

        private void ButtonOpDivide_Click(object sender, RoutedEventArgs e)
        {
            doComputation();
            currentOperator = BinaryOperator.Divide;
            RefreshOperatorButtons();
        }


        private void ButtonOpEqulas_Click(object sender, RoutedEventArgs e)
        {
            if (currentOperator != BinaryOperator.None && IsCurrentEntryEmpty()) {
                var val = GetCurrentDisplayNumber();
                var currentValue = basicCalulator.doBinaryOperation(currentOperator, val);
                isShowingResult = true;
                RefreshNumberDisplay();
            } else {
                doComputation();
                currentOperator = BinaryOperator.None;  // ????
            }
            RefreshOperatorButtons();
        }



        private void ButtonMiscDelete_Click(object sender, RoutedEventArgs e)
        {
            if (isShowingResult) {
                // Do nothing.
            } else {
                if (IsCurrentDisplayEmptyOrZero() == true || currentEntryStr.Length == 1) {
                    currentEntryStr = "";
                } else {
                    currentEntryStr = currentEntryStr.Substring(0, currentEntryStr.Length - 1);
                }
                RefreshNumberDisplay();
            }
        }

        private void ButtonMiscClear_Click(object sender, RoutedEventArgs e)
        {
            basicCalulator.Clear();
            currentOperator = BinaryOperator.None;
            currentEntryStr = "";
            isShowingResult = false;
            RefreshNumberDisplay();
            RefreshOperatorButtons();
        }

        private void ButtonMiscClearEntry_Click(object sender, RoutedEventArgs e)
        {
            // [1] This is more in line with the Windows calculator behavior
            //// currentOperator = BinaryOperator.None;
            //currentEntryStr = "";
            //isShowingResult = false;
            //RefreshNumberDisplay();
            //// RefreshOperatorButtons();
            // [2] This seems more reasonable, however.
            if (currentOperator == BinaryOperator.None) {
                currentEntryStr = "";
                isShowingResult = false;
            } else {
                currentEntryStr = "";
                isShowingResult = true;
            }
            RefreshNumberDisplay();
        }


        // TBD:
        private void Undo()
        {
            // ...
            // basicCalulator.Undo();
            // currentOperator = previousOperator;
            // RefreshNumberDisplay();
            // RefreshOperatorButtons();
            // ...
        }

        private void RefreshNumberDisplay()
        {
            if (isShowingResult) {
                TextBoxNumberDisplay.Text = basicCalulator.CurrentValue.ToString();
            } else {
                if (IsCurrentEntryEmpty()) {
                    TextBoxNumberDisplay.Text = "0";
                } else {
                    TextBoxNumberDisplay.Text = currentEntryStr;
                }
            }

        }

        // TBD
        private void RefreshOperatorButtons()
        {
            // Add/remove active operator indicators for binary ops
            // ...
        }

    }
}