﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleTimer.Common
{
    public enum CountdownTimerStatus
    {
        UNSET = 0,
        INITIALIZED,   // time set.
        RUNNING,
        STOPPED
    }
    public class BasicCountdownTimer
    {
        //// temporary
        //private static readonly ulong DEFAULT_DELTA = 50UL;

        // TBD: max --> min rather than countdownSeconds --> 0 ??
        private uint countdownSeconds = 0U;   // 0 is an invalid value.
        private CountdownTimerStatus status;
        private ulong currentMillis = 0UL;
        private long countdownStartTime = 0L;

        public BasicCountdownTimer() : this(0)
        {
        }
        public BasicCountdownTimer(uint countdownSeconds)
        {
            CountdownSeconds = countdownSeconds;
        }

        private void UpdateCurrentMillis()
        {
            if (status == CountdownTimerStatus.RUNNING) {
                var now = DateTimeUtil.CurrentUnixEpochMillis();
                var elapsed = now - countdownStartTime;
                var delta = (countdownSeconds * 1000L) - elapsed;
                if (delta < 0L) {
                    currentMillis = 0UL;
                } else {
                    currentMillis = (ulong) delta;
                }
            }
        }

        public uint CountdownSeconds
        {
            get
            {
                return countdownSeconds;
            }
            protected set
            {
                countdownSeconds = value;
                currentMillis = countdownSeconds * 1000UL;
                ResetStatus();
            }
        }
        private void ResetStatus()
        {
            if (this.countdownSeconds == 0) {
                status = CountdownTimerStatus.UNSET;
            } else {
                status = CountdownTimerStatus.INITIALIZED;
            }
        }

        public ulong CurrentMillis
        {
            get
            {
                UpdateCurrentMillis();
                if (currentMillis <= 0) {
                    currentMillis = 0;
                    status = CountdownTimerStatus.STOPPED;
                }
                return currentMillis;
            }
            private set
            {
                // TBD: Validation?
                currentMillis = value;
            }
        }


        // TBD:
        // Format example: http://msdn.microsoft.com/en-us/library/windows/apps/dd992632.aspx
        // ...

        public string CurrentMillisFormatted
        {
            get
            {
                TimeSpan time = CurrentMillis.ToTimeSpan();

                // Heck
                //   (when the last digit ends with "0", the "g" format does not display it) 
                var strTime = time.ToString("g");
                strTime = reformatTimeString(strTime);
                // Heck

                return strTime;
            }
        }

        private string reformatTimeString(string strTime)
        {
            // Heck
            var idx = strTime.LastIndexOf(".");
            if (idx == -1) {
                strTime = strTime + ".0";
            } else {
                if (idx < strTime.Length - 1) {
                    strTime = strTime.Substring(0, idx + 2);
                } else {
                    strTime += "0";
                }
            }
            // Heck

            return strTime;
        }

        public double CurrentMillisRatio
        {
            get
            {
                if (countdownSeconds > 0) {
                    var ratio = (double) (CurrentMillis / (double) (countdownSeconds * 1000UL));
                    return ratio;
                } else {
                    return 0.0;   // ???
                }
            }
        }
        public double CurrentMillisPercent
        {
            get
            {
                if (countdownSeconds > 0) {
                    var ratio = (double) (100.0 * (CurrentMillis / (double) (countdownSeconds * 1000UL)));
                    return ratio;
                } else {
                    return 0.0;   // ???
                }
            }
        }



        public CountdownTimerStatus Status
        {
            get
            {
                return status;
            }
        }


        //// TBD
        //internal ulong CountDown()
        //{
        //    return CountDown(DEFAULT_DELTA);
        //}
        //internal ulong CountDown(ulong delta)
        //{
        //    if (status == CountdownTimerStatus.RUNNING) {
        //        if (currentMillis > delta) {
        //            currentMillis -= delta;
        //        } else {    // if (currentMillis >= 0) {
        //            currentMillis = 0UL;
        //            // ???
        //            status = CountdownTimerStatus.STOPPED;
        //        }
        //        return currentMillis;
        //    } else {
        //        throw new InvalidOperationException("Timer is not running.");
        //    }
        //}


        public bool IsInitialized
        {
            get 
            {
                return (status != CountdownTimerStatus.UNSET);
            }
        }
        public bool IsRunning
        {
            get 
            {
                return (status == CountdownTimerStatus.RUNNING);
            }
        }


        public void Set(int minutes, int seconds)
        {
            Set(0, minutes, seconds);
        }
        public void Set(int hours, int minutes, int seconds)
        {
            TimeSpan timeSpan = new TimeSpan(hours, minutes, seconds);
            CountdownSeconds = (uint) timeSpan.ToTotalSeconds();
        }

        public void Reset()
        {
            currentMillis = this.countdownSeconds * 1000UL;
            countdownStartTime = 0L;
            ResetStatus();
        }

        public void Start()
        {
            if (status == CountdownTimerStatus.INITIALIZED) {



                status = CountdownTimerStatus.RUNNING;
                countdownStartTime = DateTimeUtil.CurrentUnixEpochMillis();
                UpdateCurrentMillis();
            } else {
                throw new InvalidOperationException("Timer is not initialized.");
            }
        }
        public void Stop()
        {
            if (status == CountdownTimerStatus.RUNNING) {



                UpdateCurrentMillis();
                status = CountdownTimerStatus.STOPPED;
            } else {
                throw new InvalidOperationException("Timer is not running.");
            }
        }
    
    }
}
