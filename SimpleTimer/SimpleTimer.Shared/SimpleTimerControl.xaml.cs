﻿using SimpleTimer.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SimpleTimer
{
    public sealed partial class SimpleTimerControl : UserControl
    {
        private BasicCountdownTimer basicCountdownTimer;

        public SimpleTimerControl()
        {
            this.InitializeComponent();

            basicCountdownTimer = new BasicCountdownTimer();
            UpdateButtonStates();

            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, object e)
        {
            if (basicCountdownTimer.IsInitialized) {
                var strTime = basicCountdownTimer.CurrentMillisFormatted;
                TextBlockTimer1.Text = strTime;
                UpdateTimerProgressBar();
                UpdateButtonStates();
            }
        }


        private void UpdateButtonStates()
        {
            var status = basicCountdownTimer.Status;
            switch (status) {
                case CountdownTimerStatus.UNSET:
                default:
                    ButtonTimerSet.IsEnabled = true;
                    ButtonTimerStart.IsEnabled = false;
                    ButtonTimerStop.IsEnabled = false;
                    ButtonTimerReset.IsEnabled = false;
                    break;
                case CountdownTimerStatus.INITIALIZED:
                    ButtonTimerSet.IsEnabled = true;
                    ButtonTimerStart.IsEnabled = true;
                    ButtonTimerStop.IsEnabled = false;
                    ButtonTimerReset.IsEnabled = false;
                    break;
                case CountdownTimerStatus.RUNNING:
                    ButtonTimerSet.IsEnabled = false;
                    ButtonTimerStart.IsEnabled = false;
                    ButtonTimerStop.IsEnabled = true;
                    ButtonTimerReset.IsEnabled = false;
                    break;
                case CountdownTimerStatus.STOPPED:
                    ButtonTimerSet.IsEnabled = true;
                    ButtonTimerStart.IsEnabled = false;
                    ButtonTimerStop.IsEnabled = false;
                    ButtonTimerReset.IsEnabled = true;
                    break;
            }
        }
        private void UpdateTimerProgressBar()
        {
            var status = basicCountdownTimer.Status;
            if (status == CountdownTimerStatus.RUNNING || status == CountdownTimerStatus.STOPPED) {
                var val = basicCountdownTimer.CurrentMillisPercent;
                ProgressBarTimer1.Value = val;
            } else if (status == CountdownTimerStatus.INITIALIZED) {
                ProgressBarTimer1.Value = 100.0;
            } else {
                ProgressBarTimer1.Value = 0.0;
            }
        }


        private void ButtonFlyoutSetTimerOK_Click(object sender, RoutedEventArgs e)
        {
            int minutes = 0;
            int seconds = 0;
            try {
                var strMintues = TextBoxSetTimerMinutes.Text;
                var strSeconds = TextBoxSetTimerSeconds.Text;
                minutes = Convert.ToInt32(strMintues);
                seconds = Convert.ToInt32(strSeconds);
            } catch (Exception) {
                // ignore
            }
            basicCountdownTimer.Set(minutes, seconds);
            UpdateButtonStates();

            ButtonTimerSet.Flyout.Hide();
        }

        private void ButtonFlyoutSetTimerCancel_Click(object sender, RoutedEventArgs e)
        {
            // Do nothing.
            ButtonTimerSet.Flyout.Hide();
        }

        private void ButtonTimerStart_Click(object sender, RoutedEventArgs e)
        {
            basicCountdownTimer.Start();
            UpdateButtonStates();
        }

        private void ButtonTimerStop_Click(object sender, RoutedEventArgs e)
        {
            basicCountdownTimer.Stop();
            UpdateButtonStates();
        }

        private void ButtonTimerReset_Click(object sender, RoutedEventArgs e)
        {
            basicCountdownTimer.Reset();
            UpdateButtonStates();
        }

    
    }
}
