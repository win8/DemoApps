﻿using SimpleClock.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SimpleClock
{
    public sealed partial class SimpleClockControl : UserControl
    {
        private BasicDigitalClock digitalClock;

        public SimpleClockControl()
        {
            this.InitializeComponent();

            digitalClock = new BasicDigitalClock();

            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0,0, 100);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, object e)
        {
            var strDate = digitalClock.DateFormatted;
            var strTime = digitalClock.TimeFormatted;

            TextBlockClockDate.Text = strDate;
            TextBlockClockTime.Text = strTime;

        }



    
    
    }
}
