﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;

namespace SimpleClock.Common
{

    public class BasicDigitalClock
    {
        // "cache" ?
        private long currentTime;

        public BasicDigitalClock()
        {
            UpdateCurrentTime();
        }

        // TBD:
        private void UpdateCurrentTime()
        {
            // ????
            currentTime = DateTimeUtil.CurrentUnixEpochMillis();
        }


        // TBD:
        // Date/Time strings
        // Format examples: http://msdn.microsoft.com/en-us/library/zdtaw1bw(v=vs.110).aspx
        // ...

        public string DateFormatted
        {
            get
            {
                UpdateCurrentTime();
                DateTime now = currentTime.ToLocalDateTime();
                //DateTime date = now.Date;
                //string strDate = date.ToString("d");
                string strDate = now.ToString("d");
                return strDate;
            }
        }
        public string TimeFormatted
        {
            get
            {
                UpdateCurrentTime();
                DateTime now = currentTime.ToLocalDateTime();
                //TimeSpan time = now.TimeOfDay;
                //string strTime = time.ToString("T");
                string strTime = now.ToString("T");
                return strTime;
            }
        }

 

    }

}
