

[ Windows Store App submission checklist ]
http://msdn.microsoft.com/en-us/library/windows/apps/hh694062.aspx


## Description 

A clock demo app. The app includes a simple digital clock, and no more.

This app is the 6th from a series of "demo apps" we are creating to practice/train Windows app development.

Note that this app is intended to be used in the "Snap View" (docked on the left- or right-hand side of the screen with a narrow width (typically, 320~500 pixels)).



## App Features

## Keywords

Digital clock
Windows universal app development
Windows 8.1 Phone app development


## Copyright & Trademark

Copyright (c) 2015, Pico Software


## Screenshots

1366 x 766

Digital clock widget.


## App website

http://www.windowsdemoapps.com/

## Support contact info

http://www.windowsdemoapps.com/support
picosoftware@outlook.com


## Privacy policy

http://www.windowsdemoapps.com/privacy


## Legal

http://www.windowsdemoapps.com/legal



