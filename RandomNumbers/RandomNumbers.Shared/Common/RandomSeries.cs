﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


namespace RandomNumbers.Common
{
    public class RandomSeries : INotifyPropertyChanged
    {
        //private int _currentValue;
        //private int[] _previousValues;

        private int _randomValue1;   // current value
        private int _randomValue2;   // previous value
        private int _randomValue3;   // previous x2 value
        private int _randomValue4;   // previous x3 value

        public int RandomValue1
        {
            get
            {
                return _randomValue1;
            }
            set
            {
                _randomValue1 = value;
                RaisePropertyChanged("RandomValue1");
            }
        }

        public int RandomValue2
        {
            get
            {
                return _randomValue2;
            }
            set
            {
                _randomValue2 = value;
                RaisePropertyChanged("RandomValue2");
            }
        }

        public int RandomValue3
        {
            get
            {
                return _randomValue3;
            }
            set
            {
                _randomValue3 = value;
                RaisePropertyChanged("RandomValue3");
            }
        }

        public int RandomValue4
        {
            get
            {
                return _randomValue4;
            }
            set
            {
                _randomValue4 = value;
                RaisePropertyChanged("RandomValue4");
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }


    }
}
