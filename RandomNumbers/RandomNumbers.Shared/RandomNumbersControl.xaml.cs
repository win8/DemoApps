﻿using RandomNumbers.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace RandomNumbers
{
    public sealed partial class RandomNumbersControl : UserControl
    {
        private static readonly Random RNG = new Random();
        private RandomSeries randomSeries;
        public RandomNumbersControl()
        {
            this.InitializeComponent();

            randomSeries = new RandomSeries();
            randomSeries.RandomValue1 = 0;
            randomSeries.RandomValue2 = 0;
            randomSeries.RandomValue3 = 0;
            randomSeries.RandomValue4 = 0;
            // ...

            // LayoutRoot.DataContext = randomSeries;
            this.DataContext = randomSeries;

            // temporary
            // TBD: Read these values from app settings.
            var min = 0;
            var max = 10;
            TextBoxMin.Text = min.ToString();
            TextBoxMax.Text = max.ToString();
            // ...
            // TBD:
            // ??? This makes the numbers not show up at all. Why???
            //TextBoxRandom1.Text = "";
            //TextBoxRandom2.Text = "";
            //TextBoxRandom3.Text = "";
            //TextBoxRandom4.Text = "";
            //// ...
        
        }

        private void ButtonGenerate_Click(object sender, RoutedEventArgs e)
        {
            // TBD: Validate input?
            int min = 0;
            try {
                min = Convert.ToInt32(TextBoxMin.Text);
            } catch (FormatException) {
                // ignore.
            }
            int max = 0;
            try {
                max = Convert.ToInt32(TextBoxMax.Text);
            } catch (FormatException) {
                // ignore.
            }

            randomSeries.RandomValue4 = randomSeries.RandomValue3;
            randomSeries.RandomValue3 = randomSeries.RandomValue2;
            randomSeries.RandomValue2 = randomSeries.RandomValue1;

            int r = RNG.Next(max - min) + min;
            randomSeries.RandomValue1 = r;

        }

    }
}
