﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UniversalBookmarks.Common;
using UniversalBookmarks.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalBookmarks
{
    public sealed partial class UniversalBookmarksControl : UserControl
    {
        public UniversalBookmarksControl()
        {
            this.InitializeComponent();

            this.Loaded += FlipViewContainerControl_Loaded;

            //// temporary
            //FlipNavigationHelper.Instance.ParentFlipView = FlipViewLayoutRoot;

        }

        private void FlipViewContainerControl_Loaded(object sender, RoutedEventArgs e)
        {
            // temporary
            FlipNavigationHelper.Instance.ParentFlipView = FlipViewLayoutRoot;

            //// tbd:
            //// Load all children/usercontrols from the xaml into FlipNavigationHelper ???
            //// --> This is done in the ParentFlipView property setter.
            //var items = FlipViewLayoutRoot.Items;
            //foreach (var i in items) {
            //    var userControl = i as UserControl;
            //    if (userControl != null) {
            //        // ...
            //    }
            //}

        }

        // ???
        public void RefreshAll()
        {
            var allControls = FlipNavigationHelper.Instance.AllUserControls;
            foreach (var u in allControls) {
                if (u is IRefreshableElement) {
                    ((IRefreshableElement) u).RefreshDataAndUI();
                }
            }
        }

        private void FlipViewLayoutRoot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FlipViewLayoutRoot != null) {
                UserControl userControl = FlipViewLayoutRoot.SelectedItem as UserControl;
                if (userControl != null) {

                    if (userControl is IRefreshableElement) {
                        ((IRefreshableElement) userControl).RefreshDataAndUI();
                    }

                    // etc...

                }
            }
        }

    }
}
