﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UniversalBookmarks.Common;
using UniversalBookmarks.Core;
using UniversalBookmarks.Util;
using UniversalBookmarks.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalBookmarks
{
    public sealed partial class BookmarkInputControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private BookmarkInputViewModel viewModel;
        public BookmarkInputControl()
        {
            this.InitializeComponent();
            // viewModel = new TweetPostViewModel();
            ResetViewModel();
            // this.DataContext = viewModel;

            // TBD:
            // RefreshDataAndUI();
        }
        private void ResetViewModel()
        {
            var vm = new BookmarkInputViewModel();

            // this.viewModel. ...

            ResetViewModel(vm);
        }
        private void ResetViewModel(BookmarkInputViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;

            RefreshDataAndUI();
        }

        public void RefreshDataAndUI()
        {
            // UpdateSaveButtonEnabled();
        }
        // Now using data binding.
        private void UpdateSaveButtonEnabled()
        {
            var url = TextBoxURL.Text;
            if (url.Length > 0) {
                ButtonSave.IsEnabled = true;
            } else {
                ButtonSave.IsEnabled = false;
            }
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (BookmarkInputViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }


        private void TextBoxURL_TextChanged(object sender, TextChangedEventArgs e)
        {
            var url = TextBoxURL.Text;
            viewModel.URL = url;
        }

        private void TextBoxTitle_TextChanged(object sender, TextChangedEventArgs e)
        {
            var title = TextBoxTitle.Text;
            viewModel.Title = title;
        }

        private void TextBoxDescription_TextChanged(object sender, TextChangedEventArgs e)
        {
            var description = TextBoxDescription.Text;
            viewModel.Description = description;
        }


        private void ButtonIcon_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            // TBD: Validate url ???
            // TBD: Allow url input text without leading "http://" ???
            if (!BookmarksCommonUtil.IsURL(viewModel.URL)) {
                ToastNotificationHelper.Instance.ShowToast("URL is invalid.");
                return;
            }

            var id = viewModel.BookmarkId;
            var bookmark = new BookmarkLink(id, viewModel.URL, viewModel.Title, viewModel.Description);

            // TBD:
            // Icon ???
            // ...

            // TBD:
            // If the bookmark already exists,
            // jus "reuse" the existing one????
            // ....

            BookmarkDataManager.Instance.StoreBookmarkLink(bookmark);
        }

        private void ButtonDisable_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            var id = viewModel.BookmarkId;
            if (id > 0UL) {
                var suc = BookmarkDataManager.Instance.DeleteBookmarkLink(id);
                if (suc) {
                    ResetViewModel();
                }
            } else {
                ResetViewModel();
            }
        }

        private void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            ResetViewModel();
        }


    }
}