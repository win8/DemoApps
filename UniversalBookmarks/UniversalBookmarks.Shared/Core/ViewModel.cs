﻿using System;
using System.Collections.Generic;
using System.Text;


namespace UniversalBookmarks.Core
{
    public interface IViewModel
    {
    }

    public interface IViewModelHolder
    {
        IViewModel ViewModel
        {
            get;
            set;
        }
    }


}
