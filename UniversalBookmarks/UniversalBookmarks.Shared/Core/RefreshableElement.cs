﻿using System;
using System.Collections.Generic;
using System.Text;


namespace UniversalBookmarks.Core
{
    public interface IRefreshableContainer
    {
        // Refresh all refreshables.
        void RefreshAll();
    }


    public interface IRefreshableElement
    {
        void RefreshDataAndUI();
        // void RefreshDataAndUI(IViewModel viewModel);
        // void RefreshDataAndUI(ICoreDataObject coreDataObject);
    }

    public class AbstractRefreshableElement : IRefreshableElement
    {
        public void RefreshDataAndUI()
        {
            // Do nothing.
        }
    }

}
