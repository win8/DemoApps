﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;


namespace UniversalBookmarks.Common
{
    public enum AppDataType
    {
        Local,
        Roaming
    }

    public sealed class AppDataHelper
    {
        // temporary
        private static readonly string KEY_APP_DATA_TYPE = "AppDataType";
        private static readonly AppDataType DEFAULT_SETTINGS_TYPE = AppDataType.Local;


        private static AppDataHelper instance = null;
        public static AppDataHelper Instance
        {
            get 
            {
                if (instance == null) {
                    instance = new AppDataHelper();
                }
                return instance;
            }
        }
        private AppDataHelper()
        {
        }



        public void StoreAppDataType(AppDataType appDataType)
        {
            // TBD: Check RoamingSettings quota first???

            var st = false;
            switch (appDataType) {
                case AppDataType.Local:
                default:
                    st = false;
                    break;
                case AppDataType.Roaming:
                    st = true;
                    break;
            }

            ApplicationData.Current.LocalSettings.Values[KEY_APP_DATA_TYPE] = st;
        }

        public AppDataType GetAppDataType()
        {
            var AppDataType = DEFAULT_SETTINGS_TYPE;
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(KEY_APP_DATA_TYPE)) {
                bool st = (bool) ApplicationData.Current.LocalSettings.Values[KEY_APP_DATA_TYPE];
                if (st) {
                    AppDataType = AppDataType.Roaming;
                } else {
                    AppDataType = AppDataType.Local;
                }
            }
            return AppDataType;
        }


        public ApplicationDataContainer GetAppSettings()
        {
            ApplicationDataContainer applicationDataContainer;
            AppDataType appDataType = GetAppDataType();
            switch (appDataType) {
                case AppDataType.Local:
                default:
                    applicationDataContainer = ApplicationData.Current.LocalSettings;
                    break;
                case AppDataType.Roaming:
                    applicationDataContainer = ApplicationData.Current.RoamingSettings;
                    break;
            }
            return applicationDataContainer;
        }

        public StorageFolder GetAppDataFolder()
        {
            StorageFolder StorageFolder;
            AppDataType appDataType = GetAppDataType();
            switch (appDataType) {
                case AppDataType.Local:
                default:
                    StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                    break;
                case AppDataType.Roaming:
                    StorageFolder = Windows.Storage.ApplicationData.Current.RoamingFolder;
                    break;
            }
            return StorageFolder;
        }



        // TBD:

        // Copy local to roaming
        // Copy roaming to local
        // ...


    }

}
