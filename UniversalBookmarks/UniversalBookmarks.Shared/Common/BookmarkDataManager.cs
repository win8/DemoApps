﻿using UniversalBookmarks.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace UniversalBookmarks.Common
{
    public class BookmarkDataManager
    {
        private static BookmarkDataManager instance = null;
        public static BookmarkDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new BookmarkDataManager();
                }
                return instance;
            }
        }
        private BookmarkDataManager()
        {
        }

        
        // TBD:
        public List<BookmarkLink> GetAllBookmarkLinks()
        {
            return GetAllBookmarkLinks(-1);
        }
        public List<BookmarkLink> GetAllBookmarkLinks(int size)
        {
            var bookmarks = new List<BookmarkLink>();

            var profs = BookmarkLinkSettingsHelper.Instance.GetAllBookmarks();
            if (size > 0) {
                var len = profs.Count;
                var limit = Math.Min(len, size);
                bookmarks = profs.GetRange(0, limit);
            } else {
                bookmarks = profs;
            }

            return bookmarks;
        }

        public List<BookmarkLink> GetActiveBookmarkLinks()
        {
            return GetActiveBookmarkLinks(-1);
        }
        public List<BookmarkLink> GetActiveBookmarkLinks(int size)
        {
            var bookmarks = new List<BookmarkLink>();

            var profs = BookmarkLinkSettingsHelper.Instance.GetActiveBookmarks();
            if (size > 0) {
                var len = profs.Count;
                var limit = Math.Min(len, size);
                bookmarks = profs.GetRange(0, limit);
            } else {
                bookmarks = profs;
            }

            return bookmarks;
        }



        // Create or Update.
        public BookmarkLink StoreBookmarkLink(BookmarkLink bookmark)
        {
            var id = bookmark.Id;
            if (id == 0UL) {
                // Error !!
                // what to do ???
                id = IdUtil.GetNextUniqueId();
                bookmark.Id = id;
            }

            // TBD:
            var strUrl = bookmark.URL;
            if (!BookmarksCommonUtil.IsURL(strUrl)) {
                // What to do???
                // ...
            }

            var now = DateTimeUtil.CurrentUnixEpochMillis();
            var created = bookmark.Created;
            if (created == 0L) {
                bookmark.Created = now;
            }
            bookmark.Updated = now;

            // Store metadata.
            BookmarkLinkSettingsHelper.Instance.StoreBookmark(bookmark);

            return bookmark;
        }

        public bool ExistsBookmarkLink(ulong id)
        {
            var exists = false;

            exists = BookmarkLinkSettingsHelper.Instance.ContainsBookmark(id);
            if (exists) {
                // TBD:
                // Check the content as well??
                // Or, just checking the metadata is enough?
            }

            return exists;
        }

        public BookmarkLink? FetchBookmarkLink(ulong id)
        {
            BookmarkLink? bookmark = BookmarkLinkSettingsHelper.Instance.FetchBookmark(id);
            System.Diagnostics.Debug.WriteLine("FetchBookmarkLink() id = {0}, bookmark = {1}", id, bookmark);

            return bookmark;
        }

        public bool DeleteBookmarkLink(ulong id)
        {
            var suc = BookmarkLinkSettingsHelper.Instance.DeleteBookmark(id);
            return suc;
        }




    }

}
