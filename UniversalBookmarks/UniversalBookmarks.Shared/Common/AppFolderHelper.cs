﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;


namespace UniversalBookmarks.Common
{
    public enum AppFolderType
    {
        Local,
        Roaming
    }

    public sealed class AppFolderHelper
    {
        // temporary
        private static readonly string KEY_APP_FOLDER_TYPE = "AppFolderType";
        private static readonly AppFolderType DEFAULT_SETTINGS_TYPE = AppFolderType.Local;


        private static AppFolderHelper instance = null;
        public static AppFolderHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new AppFolderHelper();
                }
                return instance;
            }
        }
        private AppFolderHelper()
        {
        }



        public void StoreAppFolderType(AppFolderType appFolderType)
        {
            // TBD: Check RoamingSettings quota first???

            var st = false;
            switch (appFolderType) {
                case AppFolderType.Local:
                default:
                    st = false;
                    break;
                case AppFolderType.Roaming:
                    st = true;
                    break;
            }

            ApplicationData.Current.LocalSettings.Values[KEY_APP_FOLDER_TYPE] = st;
        }

        public AppFolderType GetAppFolderType()
        {
            var AppFolderType = DEFAULT_SETTINGS_TYPE;
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(KEY_APP_FOLDER_TYPE)) {
                bool st = (bool) ApplicationData.Current.LocalSettings.Values[KEY_APP_FOLDER_TYPE];
                if (st) {
                    AppFolderType = AppFolderType.Roaming;
                } else {
                    AppFolderType = AppFolderType.Local;
                }
            }
            return AppFolderType;
        }


        public StorageFolder GetAppDataFolder()
        {
            StorageFolder StorageFolder;
            AppFolderType AppFolderType = GetAppFolderType();
            switch (AppFolderType) {
                case AppFolderType.Local:
                default:
                    StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                    break;
                case AppFolderType.Roaming:
                    StorageFolder = Windows.Storage.ApplicationData.Current.RoamingFolder;
                    break;
            }
            return StorageFolder;
        }



        // TBD:

        // Copy local to roaming
        // Copy roaming to local
        // ...


    }

}
