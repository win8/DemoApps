﻿using System;
using System.Collections.Generic;
using System.Text;


namespace UniversalBookmarks.Common
{
    // A Bookmark
    public struct BookmarkLink
    {
        private ulong id;
        private string url;
        private string title;
        private string description;
        private bool disabled;
        private long created;
        private long updated;

        public BookmarkLink(ulong id) : this(id, null)
        {
        }
        public BookmarkLink(ulong id, string url)
            : this(id, url, null, null)
        {
        }
        public BookmarkLink(ulong id, string url, string title, string description)
            : this(id, url, title, description, false, 0L, 0L)
        {
        }
        public BookmarkLink(ulong id, string url, string title, string description, bool disabled, long created, long updated)
        {
            this.id = id;
            this.url = url;
            this.title = title;
            this.description = description;
            this.disabled = disabled;
            this.created = created;
            this.updated = updated;
        }

        public ulong Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string URL
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        public string TitleOrURL
        {
            get
            {
                if (String.IsNullOrEmpty(title)) {
                    // title = String.Format("({0})", url);
                    title = url;
                }
                return title;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public bool IsDisabled
        {
            get
            {
                return disabled;
            }
            set
            {
                disabled = value;
            }
        }

        public long Created
        {
            get
            {
                return created;
            }
            set
            {
                created = value;
            }
        }
        public long Updated
        {
            get
            {
                return updated;
            }
            set
            {
                updated = value;
            }
        }


        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Id:").Append(Id).Append("; ");
            sb.Append("URL:").Append(URL).Append("; ");
            sb.Append("Title:").Append(Title).Append("; ");
            sb.Append("Description:").Append(Description).Append("; ");
            sb.Append("IsDisabled: ").Append(IsDisabled).Append("; ");
            sb.Append("Created: ").Append(Created).Append("; ");
            sb.Append("Updated: ").Append(Updated);
            return sb.ToString();
        }

    }


}
