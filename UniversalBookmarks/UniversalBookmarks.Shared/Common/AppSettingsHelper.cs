﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;


namespace UniversalBookmarks.Common
{
    public enum AppSettingsType
    {
        Temporary,
        Local,
        Roaming
    }

    public sealed class AppSettingsHelper
    {
        // temporary
        private static readonly string KEY_APP_SETTINGS_TYPE = "AppSettingsType";
        private static readonly AppSettingsType DEFAULT_SETTINGS_TYPE = AppSettingsType.Local;


        private static AppSettingsHelper instance = null;
        public static AppSettingsHelper Instance
        {
            get 
            {
                if (instance == null) {
                    instance = new AppSettingsHelper();
                }
                return instance;
            }
        }
        private AppSettingsHelper()
        {
        }



        public void StoreSettingsType(AppSettingsType settingsType)
        {
            // TBD: Check RoamingSettings quota first???

            var st = false;
            switch (settingsType) {
                case AppSettingsType.Local:
                default:
                    st = false;
                    break;
                case AppSettingsType.Roaming:
                    st = true;
                    break;
            }

            ApplicationData.Current.LocalSettings.Values[KEY_APP_SETTINGS_TYPE] = st;
        }

        public AppSettingsType GetSettingsType()
        {
            var settingsType = DEFAULT_SETTINGS_TYPE;
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(KEY_APP_SETTINGS_TYPE)) {
                bool st = (bool) ApplicationData.Current.LocalSettings.Values[KEY_APP_SETTINGS_TYPE];
                if (st) {
                    settingsType = AppSettingsType.Roaming;
                } else {
                    settingsType = AppSettingsType.Local;
                }
            }
            return settingsType;
        }


        public ApplicationDataContainer GetAppSettings()
        {
            ApplicationDataContainer applicationDataContainer;
            AppSettingsType settingsType = GetSettingsType();
            switch(settingsType) {
                case AppSettingsType.Local:
                default:
                    applicationDataContainer = ApplicationData.Current.LocalSettings;
                    break;
                case AppSettingsType.Roaming:
                    applicationDataContainer = ApplicationData.Current.RoamingSettings;
                    break;
            }
            return applicationDataContainer;
        }



        // TBD:

        // Copy local to roaming
        // Copy roaming to local
        // ...


    }

}
