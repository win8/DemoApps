﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Search;


namespace UniversalBookmarks.Common
{
    // TBD:
    // Use a subfolder...
    public class BookmarkLinkDataHelper
    {
        // temporary
        private static readonly string TWEET_IMAGE_FILENAME_PREFIX = "Image-";

        private static BookmarkLinkDataHelper instance = null;
        public static BookmarkLinkDataHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new BookmarkLinkDataHelper();
                }
                return instance;
            }
        }
        private BookmarkLinkDataHelper()
        {
        }


    }

}
