﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Windows.Storage;
using UniversalBookmarks.Util;


namespace UniversalBookmarks.Common
{
    // for tweet drafts (outgoing),
    //     pinned titles,
    //  and my posts (either only those updated via this app, or all my titles from Twitter)

    public sealed class BookmarkLinkSettingsHelper
    {
        private static readonly string KEY_BOOKMARK_CONTAINER_PREFIX = "Bookmark_Container_";
        private static readonly string BMK_FIELD_ID = "Id";
        private static readonly string BMK_FIELD_URL = "URL";
        private static readonly string BMK_FIELD_TITLE = "Title";
        private static readonly string BMK_FIELD_DESCRIPTION = "Description";
        private static readonly string BMK_FIELD_DISABLED = "Disabled";
        private static readonly string BMK_FIELD_CREATED_TIME = "Created";
        private static readonly string BMK_FIELD_UPDATED_TIME = "Updated";


        private static BookmarkLinkSettingsHelper instance = null;
        public static BookmarkLinkSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new BookmarkLinkSettingsHelper();
                }
                return instance;
            }
        }
        private BookmarkLinkSettingsHelper()
        {
        }


        private static string GenerateBookmarkContainerKey(ulong id)
        {
            var key = KEY_BOOKMARK_CONTAINER_PREFIX + id;
            return key;
        }
        private static ulong ParseIdFromBookmarkContainerKey(string key)
        {
            var id = 0UL;
            try {
                var strId = key.Substring(KEY_BOOKMARK_CONTAINER_PREFIX.Length);
                id = Convert.ToUInt64(strId);
            } catch (Exception) {
                // Ignore.
            }
            return id;
        }


        // TBD:
        public List<BookmarkLink> GetAllBookmarks()
        {
            var bookmarks = new List<BookmarkLink>();
            var containers = AppDataHelper.Instance.GetAppSettings().Containers;
            foreach (var key in containers.Keys) {
                // var dm = containers[key];

                var id = ParseIdFromBookmarkContainerKey(key);
                var dm = FetchBookmark(id);
                if (dm != null) {
                    bookmarks.Add(dm.Value);
                }
            }

            // ????
            List<BookmarkLink> sortedList = bookmarks.OrderByDescending(o => o.Created).ToList();
            // ...

            return sortedList;
        }

        // count arg?
        public List<BookmarkLink> GetActiveBookmarks()
        {
            return GetActiveBookmarks(0UL);
        }
        public List<BookmarkLink> GetActiveBookmarks(ulong userId)
        {
            var bookmarks = new List<BookmarkLink>();
            var containers = AppDataHelper.Instance.GetAppSettings().Containers;
            foreach (var key in containers.Keys) {
                var id = ParseIdFromBookmarkContainerKey(key);
                var dm = FetchBookmark(id);
                bookmarks.Add(dm.Value);
            }
            // ????
            // var now = DateTimeUtil.CurrentUnixEpochMillis();
            // List<Bookmark> sortedList = bookmarks.FindAll(o => (now < o.CreatedTime && o.UpdatedTime == 0L)).OrderBy(o => o.CreatedTime).ToList();
            List<BookmarkLink> sortedList = bookmarks.FindAll(o => (o.IsDisabled == false)).OrderByDescending(o => o.Created).ToList();
            // ...
            return sortedList;
        }




        public void StoreBookmark(BookmarkLink bookmark)
        {
            var id = bookmark.Id;
            if (id == 0UL) {
                id = IdUtil.GetNextUniqueId();
                bookmark.Id = id;
            }
            var key = GenerateBookmarkContainerKey(id);
            var appSettings = AppDataHelper.Instance.GetAppSettings();

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[BMK_FIELD_ID] = bookmark.Id;
            appSettings.Containers[key].Values[BMK_FIELD_URL] = bookmark.URL;
            appSettings.Containers[key].Values[BMK_FIELD_TITLE] = bookmark.Title;
            appSettings.Containers[key].Values[BMK_FIELD_DESCRIPTION] = bookmark.Description;
            appSettings.Containers[key].Values[BMK_FIELD_DISABLED] = bookmark.IsDisabled;
            appSettings.Containers[key].Values[BMK_FIELD_CREATED_TIME] = bookmark.Created;
            appSettings.Containers[key].Values[BMK_FIELD_UPDATED_TIME] = bookmark.Updated;
        }

        public bool ContainsBookmark(ulong id)
        {
            var key = GenerateBookmarkContainerKey(id);
            return AppDataHelper.Instance.GetAppSettings().Containers.ContainsKey(key);
        }

        public BookmarkLink? FetchBookmark(ulong id)
        {
            BookmarkLink? Bookmark = null;
            var key = GenerateBookmarkContainerKey(id);
            var appSettings = AppDataHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                // var strId = appSettings.Containers[key].Values[BMK_FIELD_ID].ToString();

                BookmarkLink dm = new BookmarkLink(id);

                var url = "";
                if (appSettings.Containers[key].Values.ContainsKey(BMK_FIELD_URL) && appSettings.Containers[key].Values[BMK_FIELD_URL] != null) {
                    url = appSettings.Containers[key].Values[BMK_FIELD_URL].ToString();
                }
                dm.URL = url;

                var title = "";
                if (appSettings.Containers[key].Values.ContainsKey(BMK_FIELD_TITLE) && appSettings.Containers[key].Values[BMK_FIELD_TITLE] != null) {
                    title = appSettings.Containers[key].Values[BMK_FIELD_TITLE].ToString();
                }
                dm.Title = title;

                var description = "";
                if (appSettings.Containers[key].Values.ContainsKey(BMK_FIELD_DESCRIPTION) && appSettings.Containers[key].Values[BMK_FIELD_DESCRIPTION] != null) {
                    description = appSettings.Containers[key].Values[BMK_FIELD_DESCRIPTION].ToString();
                }
                dm.Description = description;

                var disabled = false;
                if (appSettings.Containers[key].Values.ContainsKey(BMK_FIELD_DISABLED) && appSettings.Containers[key].Values[BMK_FIELD_DISABLED] != null) {
                    try {
                        var strDisabled = appSettings.Containers[key].Values[BMK_FIELD_DISABLED].ToString();
                        disabled = Convert.ToBoolean(strDisabled);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.IsDisabled = disabled;

                var created = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(BMK_FIELD_CREATED_TIME) && appSettings.Containers[key].Values[BMK_FIELD_CREATED_TIME] != null) {
                    try {
                        var strCreated = appSettings.Containers[key].Values[BMK_FIELD_CREATED_TIME].ToString();
                        created = Convert.ToInt64(strCreated);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.Created = created;

                var updated = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(BMK_FIELD_UPDATED_TIME) && appSettings.Containers[key].Values[BMK_FIELD_UPDATED_TIME] != null) {
                    try {
                        var strUpdated = appSettings.Containers[key].Values[BMK_FIELD_UPDATED_TIME].ToString();
                        updated = Convert.ToInt64(strUpdated);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.Updated = updated;

                Bookmark = dm;
            }
            return Bookmark;
        }

        public bool DeleteBookmark(ulong id)
        {
            var suc = false;
            var key = GenerateBookmarkContainerKey(id);
            var appSettings = AppDataHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                appSettings.DeleteContainer(key);
                suc = true;
            }
            return suc;
        }


    }

}
