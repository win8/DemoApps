﻿using UniversalBookmarks.Core;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Controls;


namespace UniversalBookmarks.Common
{
    public sealed class FlipNavigationHelper
    {
        private static FlipNavigationHelper instance = null;
        public static FlipNavigationHelper Instance
        {
            get 
            {
                if (instance == null) {
                    instance = new FlipNavigationHelper();
                }
                return instance;
            }
        }

        // temporary
        // Reference to the parent.
        private FlipView parentFlipView = null;

        // All currently included controls.
        private List<UserControl> allControls;

        // Note that some of the following might have been (temporarily) commented out.
        // We should not assume any of these controls are non-null.
        private BookmarkListControl bookmarkListControl = null;
        private BookmarkInputControl bookmarkInputControl = null;

        private FlipNavigationHelper()
        {
        }

        public FlipView ParentFlipView
        {
            get
            {
                return parentFlipView;
            }
            set
            {
                parentFlipView = value;

                // Do this only for the "first time" ???
                // Or, always reset ????

                // temporary
                var items = parentFlipView.Items;
                allControls = new List<UserControl>();
                foreach (var item in items) {
                    // System.Diagnostics.Debug.WriteLine("item = " + item);
                    var userControl = item as UserControl;
                    if(userControl != null) {
                        allControls.Add(userControl);
                        if (item is BookmarkInputControl) {
                            bookmarkInputControl = (BookmarkInputControl) userControl;
                        } else if (item is BookmarkListControl) {
                            bookmarkListControl = (BookmarkListControl) userControl;
                        } else {
                            // ???
                            System.Diagnostics.Debug.WriteLine("Unknown userControl = " + userControl);
                        }
                    } else {
                        // ???
                        System.Diagnostics.Debug.WriteLine("Non-UserControl item = " + item);
                    }
                }
            }
        }

        public List<UserControl> AllUserControls
        {
            get
            {
                return allControls;
            }
        }

        public BookmarkInputControl UserControlBookmarkInput
        {
            get
            {
                return bookmarkInputControl;
            }
        }
        public BookmarkListControl UserControlBookmarkList
        {
            get
            {
                return bookmarkListControl;
            }
        }


        private void SelectView(UserControl userControl)
        {
            if (parentFlipView != null) {
                // TBD: 
                // If we don't set the SelectedItem to null first, the flip does not work on WP for some reason.
                parentFlipView.SelectedItem = null;
                parentFlipView.SelectedItem = userControl;
                // ....
            }
        }

        public void SelectBookmarkInputControl()
        {
            SelectBookmarkInputControl(null);
        }
        public void SelectBookmarkInputControl(IViewModel viewModel)
        {
            if (bookmarkInputControl != null) {
                if (viewModel != null) {
                    bookmarkInputControl.ViewModel = viewModel;
                }
                SelectView(bookmarkInputControl);
            }
        }

        public void SelectBookmarkListControl()
        {
            if (bookmarkListControl != null) {
                SelectView(bookmarkListControl);
            }
        }


    }
}
