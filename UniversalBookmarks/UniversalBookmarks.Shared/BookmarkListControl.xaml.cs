﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UniversalBookmarks.Common;
using UniversalBookmarks.Core;
using UniversalBookmarks.Util;
using UniversalBookmarks.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalBookmarks
{
    public sealed partial class BookmarkListControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        // temporary
        private const int MAX_ITEMS = 100;
        // temporary

        private BookmarkListViewModel viewModel;
        public BookmarkListControl()
        {
            this.InitializeComponent();

            ResetViewModel();

            // tbd...
            UpdateItemListGrid();
        }
        private void ResetViewModel()
        {
            var vm = new BookmarkListViewModel();

            // This should be done in the viewModel object???
            var bookmarks = BookmarkDataManager.Instance.GetActiveBookmarkLinks(MAX_ITEMS);
            vm.Bookmarks = new ObservableCollection<BookmarkLink>(bookmarks);
            // ???

            ResetViewModel(vm);
        }
        private void ResetViewModel(BookmarkListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
            // 
            var bookmarks = BookmarkDataManager.Instance.GetActiveBookmarkLinks(MAX_ITEMS);
            this.viewModel.Bookmarks = new ObservableCollection<BookmarkLink>(bookmarks);
            // ???

            // tbd...
            UpdateItemListGrid();
        }

        private void UpdateItemListGrid()
        {
            if (viewModel.Bookmarks != null && viewModel.Bookmarks.Count > 0) {
                GridNewBookmarkButton.Visibility = Visibility.Collapsed;
                GridBookmarkListView.Visibility = Visibility.Visible;
            } else {
                GridNewBookmarkButton.Visibility = Visibility.Visible;
                GridBookmarkListView.Visibility = Visibility.Collapsed;
            }

            //// testing...
            //GridNewBookmarkButton.Visibility = Visibility.Visible;
            //GridBookmarkListView.Visibility = Visibility.Collapsed;
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (BookmarkListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }



        private async void ListViewBookmarkList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (BookmarkLink) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Bookmark selected: {0}", clickedItem);

            var url = clickedItem.URL;
            if (url != null && BookmarksCommonUtil.IsURL(url)) {
                await Launcher.LaunchUriAsync(new Uri(url));
            }
        }


        private async void ButtonItemOpen_Click(object sender, RoutedEventArgs e)
        {
            var originalSource = e.OriginalSource;
            System.Diagnostics.Debug.WriteLine("Item Open button clicked: {0}", originalSource);

            var button = sender as Button;
            if (button != null) {
                var commandParameter = button.CommandParameter;
                System.Diagnostics.Debug.WriteLine("commandParameter = {0}", commandParameter);
                if (commandParameter != null) {
                    var id = (ulong) commandParameter;
                    var bmVar = viewModel.GetBookmark(id);
                    if (bmVar != null) {
                        var bookmarkLink = bmVar.Value;
                        var url = bookmarkLink.URL;
                        if (url != null && BookmarksCommonUtil.IsURL(url)) {
                            await Launcher.LaunchUriAsync(new Uri(url));
                        }
                    }
                }
            }
        }

        private void ButtonItemEdit_Click(object sender, RoutedEventArgs e)
        {
            var originalSource = e.OriginalSource;
            System.Diagnostics.Debug.WriteLine("Item Edit button clicked: {0}", originalSource);

            var button = sender as Button;
            if (button != null) {
                var commandParameter = button.CommandParameter;
                System.Diagnostics.Debug.WriteLine("commandParameter = {0}", commandParameter);

                if (commandParameter != null) {
                    var id = (ulong) commandParameter;
                    var bmVar = viewModel.GetBookmark(id);
                    if (bmVar != null) {
                        var bookmarkLink = bmVar.Value;
                        var bmId = bookmarkLink.Id;
                        var bmUrl = bookmarkLink.URL;
                        var bmTitle = bookmarkLink.Title;
                        var bmDescription = bookmarkLink.Description;

                        var vm = new BookmarkInputViewModel(bmId);
                        vm.URL = bmUrl;
                        vm.Title = bmTitle;
                        vm.Description = bmDescription;

                        FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
                    } else {
                        var vm = new BookmarkInputViewModel();  // ????
                        FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
                    }
                }
            }
        }

        private void ButtonNewBookmark_Click(object sender, RoutedEventArgs e)
        {
            var vm = new BookmarkInputViewModel();  // ????
            FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
        }
  
    
    }
}