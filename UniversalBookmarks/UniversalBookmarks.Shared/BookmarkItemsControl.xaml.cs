﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UniversalBookmarks.Common;
using UniversalBookmarks.Core;
using UniversalBookmarks.Util;
using UniversalBookmarks.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalBookmarks
{
    public sealed partial class BookmarkItemsControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private BookmarkItemsViewModel viewModel;
        public BookmarkItemsControl()
        {
            this.InitializeComponent();

            ResetViewModel();

            // We cannot call this here anymore....
            // RefreshDataAndUI();
        }
        private void ResetViewModel()
        {
            var vm = new BookmarkItemsViewModel();

            // This should be done in the viewModel object???
            var bookmarks = BookmarkDataManager.Instance.GetActiveBookmarkLinks(6);
            vm.Bookmarks = bookmarks;
            // ???

            ResetViewModel(vm);
        }
        private void ResetViewModel(BookmarkItemsViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
            // 
            var bookmarks = BookmarkDataManager.Instance.GetActiveBookmarkLinks(6);
            this.viewModel.Bookmarks = bookmarks;
            // ???

        }



        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (BookmarkItemsViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }





        private void ButtonEdit0_Click(object sender, RoutedEventArgs e)
        {
            var link = viewModel.BookmarkLink0;
            if (link != null) {
                var bookmarkLink = link.Value;
                var bmId = bookmarkLink.Id;
                var bmUrl = bookmarkLink.URL;
                var bmTitle = bookmarkLink.Title;
                var bmDescription = bookmarkLink.Description;

                var vm = new BookmarkInputViewModel(bmId);
                vm.URL = bmUrl;
                vm.Title = bmTitle;
                vm.Description = bmDescription;

                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            } else {
                var vm = new BookmarkInputViewModel();  // ????
                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            }
        }

        private async void ButtonOpen0_Click(object sender, RoutedEventArgs e)
        {
            // var url = TextBoxURL0.Text;
            var link = viewModel.BookmarkLink0;
            if (link != null) {
                var url = viewModel.BookmarkURL0;
                if (url != null && BookmarksCommonUtil.IsURL(url)) {
                    await Launcher.LaunchUriAsync(new Uri(url));
                }
            }
        }

        private void ButtonEdit1_Click(object sender, RoutedEventArgs e)
        {
            var link = viewModel.BookmarkLink1;
            if (link != null) {
                var bookmarkLink = link.Value;
                var bmId = bookmarkLink.Id;
                var bmUrl = bookmarkLink.URL;
                var bmTitle = bookmarkLink.Title;
                var bmDescription = bookmarkLink.Description;

                var vm = new BookmarkInputViewModel(bmId);
                vm.URL = bmUrl;
                vm.Title = bmTitle;
                vm.Description = bmDescription;

                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            } else {
                var vm = new BookmarkInputViewModel();  // ????
                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            }
        }

        private async void ButtonOpen1_Click(object sender, RoutedEventArgs e)
        {
            // var url = TextBoxURL1.Text;
            var link = viewModel.BookmarkLink1;
            if (link != null) {
                var url = viewModel.BookmarkURL1;
                if (url != null && BookmarksCommonUtil.IsURL(url)) {
                    await Launcher.LaunchUriAsync(new Uri(url));
                }
            }
        }

        private void ButtonEdit2_Click(object sender, RoutedEventArgs e)
        {
            var link = viewModel.BookmarkLink2;
            if (link != null) {
                var bookmarkLink = link.Value;
                var bmId = bookmarkLink.Id;
                var bmUrl = bookmarkLink.URL;
                var bmTitle = bookmarkLink.Title;
                var bmDescription = bookmarkLink.Description;

                var vm = new BookmarkInputViewModel(bmId);
                vm.URL = bmUrl;
                vm.Title = bmTitle;
                vm.Description = bmDescription;

                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            } else {
                var vm = new BookmarkInputViewModel();  // ????
                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            }
        }

        private async void ButtonOpen2_Click(object sender, RoutedEventArgs e)
        {
            // var url = TextBoxURL2.Text;
            var link = viewModel.BookmarkLink2;
            if (link != null) {
                var url = viewModel.BookmarkURL2;
                if (url != null && BookmarksCommonUtil.IsURL(url)) {
                    await Launcher.LaunchUriAsync(new Uri(url));
                }
            }
        }

        private void ButtonEdit3_Click(object sender, RoutedEventArgs e)
        {
            var link = viewModel.BookmarkLink3;
            if (link != null) {
                var bookmarkLink = link.Value;
                var bmId = bookmarkLink.Id;
                var bmUrl = bookmarkLink.URL;
                var bmTitle = bookmarkLink.Title;
                var bmDescription = bookmarkLink.Description;

                var vm = new BookmarkInputViewModel(bmId);
                vm.URL = bmUrl;
                vm.Title = bmTitle;
                vm.Description = bmDescription;

                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            } else {
                var vm = new BookmarkInputViewModel();  // ????
                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            }
        }

        private async void ButtonOpen3_Click(object sender, RoutedEventArgs e)
        {
            // var url = TextBoxURL3.Text;
            var link = viewModel.BookmarkLink3;
            if (link != null) {
                var url = viewModel.BookmarkURL3;
                if (url != null && BookmarksCommonUtil.IsURL(url)) {
                    await Launcher.LaunchUriAsync(new Uri(url));
                }
            }
        }

        private void ButtonEdit4_Click(object sender, RoutedEventArgs e)
        {
            var link = viewModel.BookmarkLink4;
            if (link != null) {
                var bookmarkLink = link.Value;
                var bmId = bookmarkLink.Id;
                var bmUrl = bookmarkLink.URL;
                var bmTitle = bookmarkLink.Title;
                var bmDescription = bookmarkLink.Description;

                var vm = new BookmarkInputViewModel(bmId);
                vm.URL = bmUrl;
                vm.Title = bmTitle;
                vm.Description = bmDescription;

                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            } else {
                var vm = new BookmarkInputViewModel();  // ????
                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            }
        }

        private async void ButtonOpen4_Click(object sender, RoutedEventArgs e)
        {
            // var url = TextBoxURL4.Text;
            var link = viewModel.BookmarkLink4;
            if (link != null) {
                var url = viewModel.BookmarkURL4;
                if (url != null && BookmarksCommonUtil.IsURL(url)) {
                    await Launcher.LaunchUriAsync(new Uri(url));
                }
            }
        }

        private void ButtonEdit5_Click(object sender, RoutedEventArgs e)
        {
            var link = viewModel.BookmarkLink5;
            if (link != null) {
                var bookmarkLink = link.Value;
                var bmId = bookmarkLink.Id;
                var bmUrl = bookmarkLink.URL;
                var bmTitle = bookmarkLink.Title;
                var bmDescription = bookmarkLink.Description;

                var vm = new BookmarkInputViewModel(bmId);
                vm.URL = bmUrl;
                vm.Title = bmTitle;
                vm.Description = bmDescription;

                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            } else {
                var vm = new BookmarkInputViewModel();  // ????
                FlipNavigationHelper.Instance.SelectBookmarkInputControl(vm);
            }
        }

        private async void ButtonOpen5_Click(object sender, RoutedEventArgs e)
        {
            // var url = TextBoxURL5.Text;
            var link = viewModel.BookmarkLink5;
            if (link != null) {
                var url = viewModel.BookmarkURL5;
                if (url != null && BookmarksCommonUtil.IsURL(url)) {
                    await Launcher.LaunchUriAsync(new Uri(url));
                }
            }
        }


    }
}