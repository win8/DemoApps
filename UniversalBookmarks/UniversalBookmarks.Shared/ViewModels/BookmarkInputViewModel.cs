﻿using UniversalBookmarks.Core;
using UniversalBookmarks.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using Windows.UI;
using Windows.UI.Xaml.Media;


namespace UniversalBookmarks.ViewModels
{

    public class BookmarkInputViewModel : IViewModel, INotifyPropertyChanged
    {
        // temporary
        // TBD: how to reference the theme resource "DefaultTextForegroundThemeBrush" in a code?
        // ????        
        private static readonly Brush NormalTextColor = new SolidColorBrush(Colors.Gray);
        private static readonly Brush NewUrlTextColor = new SolidColorBrush(Colors.Red);

        private ulong _bookmarkId;
        private string _url;
        private string _title;
        private string _description;


        public BookmarkInputViewModel() : this(0UL)
        {
        }
        public BookmarkInputViewModel(ulong bookmarkId)
        {
            _bookmarkId = bookmarkId;

            // tbd:
            // if _bookmarkId == 0UL, set the fields with default values.
            // If _bookmarkId > 0UL, get the saved bookmark and update the fields...
            // ....

            _url = "";
            _title = "";
            _description = "";
        }


        public ulong BookmarkId
        {
            get
            {
                return _bookmarkId;
            }
        }

        public string URL
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
                RaisePropertyChanged("URL");
                RaisePropertyChanged("TextColor");
                RaisePropertyChanged("IsSaveButtonEnabled");
                RaisePropertyChanged("IsDeleteButtonEnabled");
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                RaisePropertyChanged("Title");
                RaisePropertyChanged("IsDeleteButtonEnabled");
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                RaisePropertyChanged("Description");
                RaisePropertyChanged("IsDeleteButtonEnabled");
            }
        }


 
        public Brush TextColor
        {
            get
            {
                var color = NormalTextColor;
                if (_bookmarkId == 0UL) {
                    color = NewUrlTextColor;
                }
                return color;
            }
        }

        public bool IsSaveButtonEnabled
        {
            get
            {
                var isEnabled = true;
                if (_url == null || !BookmarksCommonUtil.IsURL(_url)) {
                    isEnabled = false;
                }
                return isEnabled;
            }
        }
        public bool IsDeleteButtonEnabled
        {
            get
            {
                var isEnabled = false;
                if (!String.IsNullOrEmpty(_url) || !String.IsNullOrEmpty(_title) || !String.IsNullOrEmpty(_description)) {
                    isEnabled = true;
                }
                return isEnabled;
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

}
