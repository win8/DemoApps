﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using UniversalBookmarks.Common;
using UniversalBookmarks.Core;
using UniversalBookmarks.Util;


namespace UniversalBookmarks.ViewModels
{
    public class BookmarkItemsViewModel : IViewModel, INotifyPropertyChanged
    {

        private IList<BookmarkLink> bookmarks;
        public BookmarkItemsViewModel()
        {
            bookmarks = new List<BookmarkLink>();

            // Initialize bookmarks here?
            // Or, the caller should initialize it via the Bookmarks property setter???
            // ...
            // bookmarks = BookmarkDataManager.Instance.GetActiveBookmarkLinks(4);
            // ....


        }

        public IList<BookmarkLink> Bookmarks
        {
            get
            {
                return bookmarks;
            }
            set
            {
                bookmarks = value;
                RaisePropertyChanged("Bookmarks");

                RaisePropertyChanged("BookmarkLink0");
                RaisePropertyChanged("BookmarkURL0");
                RaisePropertyChanged("BookmarkTitle0");
                RaisePropertyChanged("BookmarkEditButtonLabel0");
                RaisePropertyChanged("BookmarkEditButtonEnabled0");
                RaisePropertyChanged("BookmarkOpenButtonEnabled0");

                RaisePropertyChanged("BookmarkLink1");
                RaisePropertyChanged("BookmarkURL1");
                RaisePropertyChanged("BookmarkTitle1");
                RaisePropertyChanged("BookmarkEditButtonLabel1");
                RaisePropertyChanged("BookmarkEditButtonEnabled1");
                RaisePropertyChanged("BookmarkOpenButtonEnabled1");

                RaisePropertyChanged("BookmarkLink2");
                RaisePropertyChanged("BookmarkURL2");
                RaisePropertyChanged("BookmarkTitle2");
                RaisePropertyChanged("BookmarkEditButtonLabel2");
                RaisePropertyChanged("BookmarkEditButtonEnabled2");
                RaisePropertyChanged("BookmarkOpenButtonEnabled2");

                RaisePropertyChanged("BookmarkLink3");
                RaisePropertyChanged("BookmarkURL3");
                RaisePropertyChanged("BookmarkTitle3");
                RaisePropertyChanged("BookmarkEditButtonLabel3");
                RaisePropertyChanged("BookmarkEditButtonEnabled3");
                RaisePropertyChanged("BookmarkOpenButtonEnabled3");

                RaisePropertyChanged("BookmarkLink4");
                RaisePropertyChanged("BookmarkURL4");
                RaisePropertyChanged("BookmarkTitle4");
                RaisePropertyChanged("BookmarkEditButtonLabel4");
                RaisePropertyChanged("BookmarkEditButtonEnabled4");
                RaisePropertyChanged("BookmarkOpenButtonEnabled4");

                RaisePropertyChanged("BookmarkLink5");
                RaisePropertyChanged("BookmarkURL5");
                RaisePropertyChanged("BookmarkTitle5");
                RaisePropertyChanged("BookmarkEditButtonLabel5");
                RaisePropertyChanged("BookmarkEditButtonEnabled5");
                RaisePropertyChanged("BookmarkOpenButtonEnabled5");
            }
        }


        // ???
        private BookmarkLink this[int index]
        {
            get
            {
                if (index < bookmarks.Count) {
                    return bookmarks[index];
                } else {
                    throw new IndexOutOfRangeException("index is too big: " + index);
                }
            }
            set
            {
                if (index < bookmarks.Count) {
                    bookmarks[index] = value;
                } else {
                    throw new IndexOutOfRangeException("index is too big: " + index);
                }
            }
        }



        // TBD:
        // Just use the indexer???
        // ....


        public BookmarkLink? BookmarkLink0
        {
            get
            {
                if (0 < bookmarks.Count) {
                    return bookmarks[0];
                } else {
                    return null;
                }
            }
        }
        public BookmarkLink? BookmarkLink1
        {
            get
            {
                if (1 < bookmarks.Count) {
                    return bookmarks[1];
                } else {
                    return null;
                }
            }
        }
        public BookmarkLink? BookmarkLink2
        {
            get
            {
                if (2 < bookmarks.Count) {
                    return bookmarks[2];
                } else {
                    return null;
                }
            }
        }
        public BookmarkLink? BookmarkLink3
        {
            get
            {
                if (3 < bookmarks.Count) {
                    return bookmarks[3];
                } else {
                    return null;
                }
            }
        }
        public BookmarkLink? BookmarkLink4
        {
            get
            {
                if (4 < bookmarks.Count) {
                    return bookmarks[4];
                } else {
                    return null;
                }
            }
        }
        public BookmarkLink? BookmarkLink5
        {
            get
            {
                if (5 < bookmarks.Count) {
                    return bookmarks[5];
                } else {
                    return null;
                }
            }
        }



        public string BookmarkURL0
        {
            get
            {
                if (0 < bookmarks.Count) {
                    return bookmarks[0].URL;
                } else {
                    return null;
                }
            }
        }
        public string BookmarkTitle0
        {
            get
            {
                if (0 < bookmarks.Count) {
                    var title = bookmarks[0].Title;
                    if (String.IsNullOrEmpty(title)) {
                        // title = String.Format("({0})", bookmarks[0].URL);
                        title = bookmarks[0].URL;
                    }
                    return title;
                } else {
                    return "";
                }
            }
        }
        public bool BookmarkEditButtonEnabled0
        {
            get
            {
                if (0 <= bookmarks.Count) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        public bool BookmarkOpenButtonEnabled0
        {
            get
            {
                if (0 < bookmarks.Count) {
                    return BookmarksCommonUtil.IsURL(bookmarks[0].URL);
                } else {
                    return false;
                }
            }
        }
        public string BookmarkEditButtonLabel0
        {
            get
            {
                if (0 == bookmarks.Count) {
                    return "New";
                } else if (0 < bookmarks.Count) {
                    return "Edit";
                } else {
                    return "New";   // ???
                }
            }
        }

        public string BookmarkURL1
        {
            get
            {
                if (1 < bookmarks.Count) {
                    return bookmarks[1].URL;
                } else {
                    return null;
                }
            }
        }
        public string BookmarkTitle1
        {
            get
            {
                if (1 < bookmarks.Count) {
                    var title = bookmarks[1].Title;
                    if (String.IsNullOrEmpty(title)) {
                        // title = String.Format("({0})", bookmarks[1].URL);
                        title = bookmarks[1].URL;
                    }
                    return title;
                } else {
                    return "";
                }
            }
        }
        public bool BookmarkEditButtonEnabled1
        {
            get
            {
                if (1 <= bookmarks.Count) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        public bool BookmarkOpenButtonEnabled1
        {
            get
            {
                if (1 < bookmarks.Count) {
                    return BookmarksCommonUtil.IsURL(bookmarks[1].URL);
                } else {
                    return false;
                }
            }
        }
        public string BookmarkEditButtonLabel1
        {
            get
            {
                if (1 == bookmarks.Count) {
                    return "New";
                } else if (1 < bookmarks.Count) {
                    return "Edit";
                } else {
                    return "New";   // ???
                }
            }
        }

        public string BookmarkURL2
        {
            get
            {
                if (2 < bookmarks.Count) {
                    return bookmarks[2].URL;
                } else {
                    return null;
                }
            }
        }
        public string BookmarkTitle2
        {
            get
            {
                if (2 < bookmarks.Count) {
                    var title = bookmarks[2].Title;
                    if (String.IsNullOrEmpty(title)) {
                        // title = String.Format("({0})", bookmarks[2].URL);
                        title = bookmarks[2].URL;
                    }
                    return title;
                } else {
                    return "";
                }
            }
        }
        public bool BookmarkEditButtonEnabled2
        {
            get
            {
                if (2 <= bookmarks.Count) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        public bool BookmarkOpenButtonEnabled2
        {
            get
            {
                if (2 < bookmarks.Count) {
                    return BookmarksCommonUtil.IsURL(bookmarks[2].URL);
                } else {
                    return false;
                }
            }
        }
        public string BookmarkEditButtonLabel2
        {
            get
            {
                if (2 == bookmarks.Count) {
                    return "New";
                } else if (2 < bookmarks.Count) {
                    return "Edit";
                } else {
                    return "New";   // ???
                }
            }
        }

        public string BookmarkURL3
        {
            get
            {
                if (3 < bookmarks.Count) {
                    return bookmarks[3].URL;
                } else {
                    return null;
                }
            }
        }
        public string BookmarkTitle3
        {
            get
            {
                if (3 < bookmarks.Count) {
                    var title = bookmarks[3].Title;
                    if (String.IsNullOrEmpty(title)) {
                        // title = String.Format("({0})", bookmarks[3].URL);
                        title = bookmarks[3].URL;
                    }
                    return title;
                } else {
                    return "";
                }
            }
        }
        public bool BookmarkEditButtonEnabled3
        {
            get
            {
                if (3 <= bookmarks.Count) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        public bool BookmarkOpenButtonEnabled3
        {
            get
            {
                if (3 < bookmarks.Count) {
                    return BookmarksCommonUtil.IsURL(bookmarks[3].URL);
                } else {
                    return false;
                }
            }
        }
        public string BookmarkEditButtonLabel3
        {
            get
            {
                if (3 == bookmarks.Count) {
                    return "New";
                } else if (3 < bookmarks.Count) {
                    return "Edit";
                } else {
                    return "New";   // ???
                }
            }
        }

        public string BookmarkURL4
        {
            get
            {
                if (4 < bookmarks.Count) {
                    return bookmarks[4].URL;
                } else {
                    return null;
                }
            }
        }
        public string BookmarkTitle4
        {
            get
            {
                if (4 < bookmarks.Count) {
                    var title = bookmarks[4].Title;
                    if (String.IsNullOrEmpty(title)) {
                        // title = String.Format("({0})", bookmarks[4].URL);
                        title = bookmarks[4].URL;
                    }
                    return title;
                } else {
                    return "";
                }
            }
        }
        public bool BookmarkEditButtonEnabled4
        {
            get
            {
                if (4 <= bookmarks.Count) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        public bool BookmarkOpenButtonEnabled4
        {
            get
            {
                if (4 < bookmarks.Count) {
                    return BookmarksCommonUtil.IsURL(bookmarks[4].URL);
                } else {
                    return false;
                }
            }
        }
        public string BookmarkEditButtonLabel4
        {
            get
            {
                if (4 == bookmarks.Count) {
                    return "New";
                } else if (4 < bookmarks.Count) {
                    return "Edit";
                } else {
                    return "New";   // ???
                }
            }
        }

        public string BookmarkURL5
        {
            get
            {
                if (5 < bookmarks.Count) {
                    return bookmarks[5].URL;
                } else {
                    return null;
                }
            }
        }
        public string BookmarkTitle5
        {
            get
            {
                if (5 < bookmarks.Count) {
                    var title = bookmarks[5].Title;
                    if (String.IsNullOrEmpty(title)) {
                        // title = String.Format("({0})", bookmarks[5].URL);
                        title = bookmarks[5].URL;
                    }
                    return title;
                } else {
                    return "";
                }
            }
        }
        public bool BookmarkEditButtonEnabled5
        {
            get
            {
                if (5 <= bookmarks.Count) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        public bool BookmarkOpenButtonEnabled5
        {
            get
            {
                if (5 < bookmarks.Count) {
                    return BookmarksCommonUtil.IsURL(bookmarks[5].URL);
                } else {
                    return false;
                }
            }
        }
        public string BookmarkEditButtonLabel5
        {
            get
            {
                if (5 == bookmarks.Count) {
                    return "New";
                } else if (5 < bookmarks.Count) {
                    return "Edit";
                } else {
                    return "New";   // ???
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
