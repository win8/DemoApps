﻿using UniversalBookmarks.Common;
using UniversalBookmarks.Core;
using UniversalBookmarks.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Collections.ObjectModel;


namespace UniversalBookmarks.ViewModels
{
    public class BookmarkListViewModel : IViewModel, INotifyPropertyChanged
    {
        // Heck: We need ordered dictionary.....
        // private IList<BookmarkLink> bookmarks;
        private ObservableCollection<BookmarkLink> bookmarks;
        private IDictionary<ulong, BookmarkLink> idMap;

        public BookmarkListViewModel()
        {
            // bookmarks = new List<BookmarkLink>();
            bookmarks = new ObservableCollection<BookmarkLink>();
            idMap = new Dictionary<ulong, BookmarkLink>();

            // Initialize bookmarks here?
            // Or, the caller should initialize it via the Bookmarks property setter???
            // ...
            // bookmarks = BookmarkDataManager.Instance.GetActiveBookmarkLinks(4);
            // ....


        }

        // public IList<BookmarkLink> Bookmarks
        public ObservableCollection<BookmarkLink> Bookmarks
        {
            get
            {
                return bookmarks;
            }
            set
            {
                bookmarks = value;
                // temporary
                idMap = new Dictionary<ulong, BookmarkLink>();
                if (bookmarks != null) {
                    foreach (var bm in bookmarks) {
                        idMap.Add(bm.Id, bm);
                    }
                }

                // ??
                RaisePropertyChanged("Bookmarks");
                // ????
            }
        }


        public BookmarkLink? GetBookmark(ulong id)
        {
            if (idMap.ContainsKey(id)) {
                return idMap[id];
            } else {
                return null;
            }
        }



        // ???
        private BookmarkLink this[int index]
        {
            get
            {
                if (index < bookmarks.Count) {
                    return bookmarks[index];
                } else {
                    throw new IndexOutOfRangeException("index is too big: " + index);
                }
            }
            set
            {
                if (index < bookmarks.Count) {
                    bookmarks[index] = value;
                } else {
                    throw new IndexOutOfRangeException("index is too big: " + index);
                }
            }
        }





        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
