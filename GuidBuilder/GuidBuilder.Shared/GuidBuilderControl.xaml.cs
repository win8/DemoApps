﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;


namespace GuidBuilder
{
    public sealed partial class GuidBuilderControl : UserControl
    {
        private string stringGuid = null;
        public GuidBuilderControl()
        {
            this.InitializeComponent();

            TextBoxGuid.Text = "";
            TextBoxGuid.Foreground = new SolidColorBrush(Colors.Black);
            if (ButtonCopy != null) {
                ButtonCopy.IsEnabled = false;
#if WINDOWS_APP
                ButtonCopy.Visibility = Visibility.Visible;
#else
            // Hide the copy-to-clipboard button on Windows Phone since it's not supported
            ButtonCopy.Visibility = Visibility.Collapsed;            
#endif
            }
        }

        private void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            var guid = Guid.NewGuid();
            stringGuid = guid.ToString();
            if (!String.IsNullOrEmpty(stringGuid)) {
                TextBoxGuid.Text = stringGuid;
                TextBoxGuid.Foreground = new SolidColorBrush(Colors.Black);
#if WINDOWS_APP
                if (ButtonCopy != null) {
                    ButtonCopy.IsEnabled = true;
                }
#endif
            } else {
                TextBoxGuid.Text = "";
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            stringGuid = "";
            TextBoxGuid.Text = stringGuid;
            TextBoxGuid.Foreground = new SolidColorBrush(Colors.Black);
#if WINDOWS_APP
            if (ButtonCopy != null) {
                ButtonCopy.IsEnabled = false;
            }
#endif
        }

        private void ButtonCopy_Click(object sender, RoutedEventArgs e)
        {
            var guidValue = TextBoxGuid.Text;
            if (!String.IsNullOrEmpty(guidValue)) {
                TextBoxGuid.Foreground = new SolidColorBrush(Colors.Gray);
#if WINDOWS_APP
                if (ButtonCopy != null) {
                    Windows.ApplicationModel.DataTransfer.DataPackage dataPackage = new Windows.ApplicationModel.DataTransfer.DataPackage();
                    dataPackage.SetText(guidValue);
                    Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(dataPackage);
                }
#endif
            }
        }

    }
}
