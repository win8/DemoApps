﻿using DemoCore.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace DemoCore.Info
{
    public sealed partial class SourceCodeInfoControl : UserControl
    {
        private SettingsFlyout parentSettingsFlyout = null;   // For Windows
        private Page parentSettingsPage = null;               // For Windows Phone.

        private string defaultEmailSubject;
        private string defaultEmailBody;

        public SourceCodeInfoControl()
        {
            this.InitializeComponent();
        }

        public SettingsFlyout ParentSettingsFlyout
        {
            get
            {
                return parentSettingsFlyout;
            }
            set
            {
                parentSettingsFlyout = value;
            }
        }
        public Page ParentSettingsPage
        {
            get
            {
                return parentSettingsPage;
            }
            set
            {
                parentSettingsPage = value;
            }
        }


        public string DefaultEmailSubject
        {
            get
            {
                return defaultEmailSubject;
            }
            set
            {
                defaultEmailSubject = value;
                TextBoxEmailSubject.Text = defaultEmailSubject;  // ???
            }
        }

        public string DefaultEmailBody
        {
            get
            {
                return defaultEmailBody;
            }
            set
            {
                defaultEmailBody = value;
                TextBoxEmailBody.Text = defaultEmailBody;  // ???
            }
        }
        

        // temporary
        public void ShowCancelButton()
        {
            ButtonSend.Width = 160;
            ButtonCancel.Width = 90;
            ButtonSend.Visibility = Visibility.Visible;
            ButtonCancel.Visibility = Visibility.Visible;
        }
        public void HideCancelButton()
        {
            ButtonSend.Width = 255;
            ButtonSend.Visibility = Visibility.Visible;
            ButtonCancel.Visibility = Visibility.Collapsed;
        }
        // temporary


        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            var subject = TextBoxEmailSubject.Text;
            var msgBody = TextBoxEmailBody.Text;
            if (!String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(msgBody)) {
                var emailShareHandler = new EmailShareHandler(subject, msgBody);
                emailShareHandler.InitiateShare(ShareFormat.Text);

                // Explicit
                Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
            } else {
                // ????
            }

            // ???
            // DismissSettingsFlyout();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            // ???
            DismissSettingsFlyout();
        }

        private void DismissSettingsFlyout()
        {
            if (parentSettingsFlyout != null) {
                parentSettingsFlyout.Hide();
            } else if (parentSettingsPage != null) {
                // ????
                // Go "back" ???
            }
        }

    }
}
